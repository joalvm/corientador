﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.42000
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'StronglyTypedResourceBuilder generó automáticamente esta clase
    'a través de una herramienta como ResGen o Visual Studio.
    'Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    'con la opción /str o recompile su proyecto de VS.
    '''<summary>
    '''  Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("COrientador.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        '''  búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Icon similar a (Icono).
        '''</summary>
        Friend ReadOnly Property favicon() As System.Drawing.Icon
            Get
                Dim obj As Object = ResourceManager.GetObject("favicon", resourceCulture)
                Return CType(obj,System.Drawing.Icon)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_delete() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_delete", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_exit_to_app() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_exit_to_app", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_language() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_language", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_lock_outline() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_lock_outline", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_perm_identity() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_perm_identity", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_print() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_print", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_search() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_search", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_action_settings() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_action_settings", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_communication_vpn_key() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_communication_vpn_key", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_add() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_add", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_block() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_block", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_clear() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_clear", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_content_copy() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_content_copy", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_create() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_create", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_filter_list() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_filter_list", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_save() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_save", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_content_send() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_content_send", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_hardware_keyboard_arrow_left() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_hardware_keyboard_arrow_left", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_hardware_keyboard_arrow_right() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_hardware_keyboard_arrow_right", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_arrow_back() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_arrow_back", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_arrow_forward() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_arrow_forward", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_check() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_check", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_close() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_close", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_menu() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_menu", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_navigation_more_vert() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_navigation_more_vert", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_notification_sync() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_notification_sync", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property icon_material_social_notifications_none() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("icon_material_social_notifications_none", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Busca un recurso adaptado de tipo System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property logo_desc() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("logo_desc", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
    End Module
End Namespace
