﻿Imports System.Collections.Specialized
Imports System.Threading
Imports Newtonsoft.Json
Imports RestSharp

Public Class Sync

	Private Query As Thread = Nothing
	Private env = Nothing
	Public params As New oParamsSync

	Private Delegate Sub check_applicationAuth(ResponseObj As oResponse)

	Private callback As Action(Of oResponse) = Nothing

	Public Sub New(ByRef environment As Form)

		env = environment

		Me.Query = New Thread(New ThreadStart(AddressOf Me.ProceedToQuerySafe))

	End Sub

	Public Sub New(ByRef environment As Control)

		env = environment

		Me.Query = New Thread(New ThreadStart(AddressOf Me.ProceedToQuerySafe))

	End Sub

	Public Sub Init(_params As oParamsSync)

		Me.params = _params

		Me.Query.Start()

	End Sub

	Private Sub ProceedToQuerySafe()

		Dim BU As String = Config.baseUrl

		If Me.params.base_url IsNot Nothing Then
			BU = Me.params.base_url
		End If

		Dim client = New RestClient(BU)
		Dim request = New RestRequest(Me.params.resource, Me.params.type)

		request.RequestFormat = Me.params.format

		'SIMULA UN AJAX
		request.AddHeader("X-Requested-With", "XMLHttpRequest")

		'VALIDA LA APLICACIÓN
		request.AddHeader("Application-Id", Config.GetSetting(Variables.IdKeyConfig))
		request.AddHeader("Application-Token", Config.GetSetting(Variables.TokenKeyConfig))
		request.AddHeader("Application-Code", Config.GetSetting(Variables.CodeKeyConfig))

		'VERIFICA LA SESIÓN INICIADA
		If Not String.IsNullOrEmpty(Config.GetSetting(Variables.UserIdKeyConfig)) Then
			request.AddHeader("Application-User", Config.GetSetting(Variables.UserIdKeyConfig))
		End If

		'VERIFICA LA SESIÓN INICIADA
		If Not String.IsNullOrEmpty(Config.GetSetting(Variables.LocalIdKeyConfig)) Then
			request.AddHeader("Local-Id", Config.GetSetting(Variables.LocalIdKeyConfig))
		End If

		If (Me.params.data IsNot Nothing) Then
			For Each key In Me.params.data.Keys
				request.AddParameter(key, Me.params.data(key))
			Next
		End If

		Dim otr As New oResponse

		Try

			Dim response = client.Execute(request)

			If response.StatusCode = Net.HttpStatusCode.InternalServerError Then
				Throw New Net.HttpListenerException(500, "Hubo un error en el servidor." + vbCrLf + "Error:" + vbCrLf + response.Content)
			End If

			If response.StatusCode = Net.HttpStatusCode.NotFound Then
				Throw New Net.HttpListenerException(404, "El recurso solicitado no se ha podido encontrar" + vbCrLf + vbCrLf + "Recurso: " + response.ResponseUri.ToString)
			End If

			otr.data = params.DataDeserialize

			JsonConvert.PopulateObject(response.Content.Trim(), otr)

			ProceedToQuery(otr)

		Catch ex As Exception

			otr.code = 400
			otr.error = True
			otr.message = ex.Message

			ProceedToQuery(otr)

		End Try



	End Sub

	Private Sub ProceedToQuery(ResponseObj As oResponse)

		If env.InvokeRequired Then

			Dim d As New check_applicationAuth(AddressOf ProceedToQuery)
			env.Invoke(d, New Object() {ResponseObj})

		Else

			'callback(ResponseObj)
			Me.params.Success(ResponseObj)

		End If

	End Sub

End Class

