﻿Imports System.Drawing.Printing
Imports System.Text

Public Class Requirement

	Private Font9 = New Font("Arial", 9, FontStyle.Regular)
	Private Font10 = New Font("Arial", 10, FontStyle.Regular)
	Private Font11 = New Font("Arial", 11, FontStyle.Regular)
	Private Font11Bold = New Font("Arial", 11, FontStyle.Bold)
	Private SolidBrush = New SolidBrush(Color.Black)
	Private widthPaper As Integer = 280

	Public requeriment As New oRequirements

	Private Sub PrintPage(sender As Object, e As PrintPageEventArgs)

		Dim gr As Graphics = e.Graphics
		Dim sizePaper As Integer = 0
		Dim position As Integer = 1
		Dim itemPositionY As Integer = 85

		Dim rectangle As Rectangle

		Dim stringTitle As New StringFormat
		Dim stringHeading As New StringFormat
		Dim stringDescription As New StringFormat
		Dim stringItem As New StringFormat

		Dim rectangleTitle As New RectangleF
		Dim rectangleHeading As New RectangleF
		Dim rectangleDescription As New RectangleF
		Dim rectangleItem As New RectangleF

		stringTitle.Alignment = StringAlignment.Center
		stringHeading.Alignment = StringAlignment.Center
		stringDescription.Alignment = StringAlignment.Near
		stringItem.Alignment = StringAlignment.Near

		rectangleTitle.Location = New Point(0, 0)
		rectangleTitle.Width = 275

		rectangleHeading.Location = New Point(0, 30)
		rectangleHeading.Width = 275

		rectangleDescription.Location = New Point(0, 60)
		rectangleDescription.Width = 275

		rectangleItem.Location = New Point(0, itemPositionY)
		rectangleItem.Width = 275

		gr.DrawString(requeriment.titulo, Font9, SolidBrush, rectangleTitle, stringTitle)
		gr.DrawString("REQUISITOS", Font11Bold, SolidBrush, rectangleHeading, stringHeading)
		gr.DrawString(requeriment.descripcion, Font9, SolidBrush, rectangleDescription, stringDescription)

		Dim listItems As New StringBuilder
		For Each item As oRequirementsList In requeriment.items

			listItems.AppendLine(position.ToString + "). " + item.requisito)
			position += 1

		Next

		gr.DrawString(listItems.ToString, Font9, SolidBrush, rectangleItem, stringItem)
		gr.DrawString("", Font11Bold, SolidBrush, rectangleHeading, stringHeading)

		sizePaper += Convert.ToInt32(e.Graphics.MeasureString(requeriment.titulo, Font11).Height)
		sizePaper += Convert.ToInt32(e.Graphics.MeasureString("REQUISITOS", Font11Bold).Height) + 20
		sizePaper += Convert.ToInt32(e.Graphics.MeasureString(requeriment.descripcion, Font9).Height) + 20
		sizePaper += Convert.ToInt32(e.Graphics.MeasureString(listItems.ToString, Font9).Height) + 20

		rectangle.Height = sizePaper
		e.PageSettings.PaperSize.Height = sizePaper
		e.PageBounds.Inflate(275, sizePaper)

	End Sub

	Public Sub init(requerimento As oRequirements)

		Me.requeriment = requerimento

		Dim pd = New PrintDocument()
		Dim pps = New PaperSize("Custom Paper Size", 275, 800)
		Dim pstg = New PrinterSettings()

		pps.RawKind = PaperKind.Custom

		pstg.DefaultPageSettings.PaperSize = pps
		pstg.DefaultPageSettings.Margins = New Margins(0, 0, 0, 0)

		pstg.PrinterName = Config.GetSetting(Variables.PrinterKeyConfig)

		AddHandler pd.PrintPage, New PrintPageEventHandler(AddressOf PrintPage)

		If pstg.IsValid Then

			pd.PrinterSettings = pstg

			pd.Print()

		End If

	End Sub
End Class
