﻿Imports System.Drawing.Printing
Imports System.Text

Public Class Ticket

	Private Space As Integer = 5
	Private Font9 = New Font(FontFamily.GenericSansSerif, 9, FontStyle.Regular)
	Private Font11 = New Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular)
	Private Font11Bold = New Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold)
	Private Font14 = New Font("Arial", 11, FontStyle.Bold)
	Private Font36 = New Font("Arial", 36, FontStyle.Bold)
	Private SolidBrush = New SolidBrush(Color.Black)

	Public [date] As String
	Public time As String
	Public servicio As String
	Public abrev As String
	Public corre As String

	Private Sub PrintPage(sender As Object, e As PrintPageEventArgs)

		'MessageBox.Show(e.PageSettings.PrintableArea.ToString)

		Dim gr As Graphics = e.Graphics
		Dim blackPen As New Pen(Color.FromArgb(255, 0, 0, 0), 1)

		Dim stringFormatDate As New StringFormat
		Dim stringFormatTime As New StringFormat
		Dim stringFormatEntidad As New StringFormat
		Dim stringFormatWelcome As New StringFormat
		Dim stringFormatService As New StringFormat
		Dim stringFormatSerie As New StringFormat
		Dim stringFormatfooter As New StringFormat

		Dim rectangleFDate As New RectangleF
		Dim rectangleFTime As New RectangleF
		Dim rectangleFEntidad As New RectangleF
		Dim rectangleFWelcome As New RectangleF
		Dim rectangleFService As New RectangleF
		Dim rectangleFSerie As New RectangleF
		Dim rectangleFFooter As New RectangleF

		stringFormatDate.Alignment = StringAlignment.Near 'Left
		stringFormatTime.Alignment = StringAlignment.Far 'Right
		stringFormatWelcome.Alignment = StringAlignment.Center
		stringFormatEntidad.Alignment = StringAlignment.Center
		stringFormatService.Alignment = StringAlignment.Center
		stringFormatSerie.Alignment = StringAlignment.Center
		stringFormatSerie.LineAlignment = StringAlignment.Center
		stringFormatfooter.Alignment = StringAlignment.Near


		rectangleFDate.Location = New Point(0, 0)
		rectangleFDate.Width = 275
		rectangleFDate.Height = 15

		rectangleFTime.Location = New Point(0, 0)
		rectangleFTime.Width = 275
		rectangleFTime.Height = 15

		rectangleFWelcome.Location = New Point(0, 25)
		rectangleFWelcome.Width = 275
		rectangleFWelcome.Height = 15

		rectangleFEntidad.Location = New Point(0, 44)
		rectangleFEntidad.Width = 275
		rectangleFEntidad.Height = 35

		rectangleFService.Location = New Point(0, 85)
		rectangleFService.Width = 275
		rectangleFService.Height = 20

		rectangleFSerie.Location = New Point(0, 113)
		rectangleFSerie.Width = 275
		rectangleFSerie.Height = 50

		rectangleFFooter.Location = New Point(0, 165)
		rectangleFFooter.Width = 275

		'DATE LEFT
		gr.DrawString("Fecha: " + Me.date, Font9, SolidBrush, rectangleFDate, stringFormatDate)

		'TIME RIGHT
		gr.DrawString("Hora: " + Me.time, Font9, SolidBrush, rectangleFTime, stringFormatTime)

		'WELCOME CENTER
		gr.DrawString("Bienvenido, a la", Font11, SolidBrush, rectangleFWelcome, stringFormatWelcome)

		'ENTIDAD CENTER
		gr.DrawString("MUNICIPALIDAD PROVINCIAL" + vbCrLf + "DEL CALLAO", Font11Bold, SolidBrush, rectangleFEntidad, stringFormatEntidad)

		'SERVICIO CENTER
		gr.DrawString(Me.servicio.ToUpper(), Font14, SolidBrush, rectangleFService, stringFormatService)

		'RECTANGUNLO
		gr.DrawRectangle(blackPen, 1, 110, 273, 50)

		'CORRELATIVO DE ATENCION
		gr.DrawString((Me.abrev + Me.corre), Font36, SolidBrush, rectangleFSerie, stringFormatSerie)

		'TEXTO DE FOOTER
		gr.DrawString("Por favor espere a que su orden de ticket aparezca en el televisor.", Font9, SolidBrush, rectangleFFooter, stringFormatfooter)



	End Sub

	Public Sub init(frm As frm_preferencias)

		Dim pd = New PrintDocument()
		Dim pps = New PaperSize("Custom Paper Size", 275, 200)
		Dim pstg = New PrinterSettings()

		pps.RawKind = PaperKind.Custom

		pstg.DefaultPageSettings.PaperSize = pps
		pstg.DefaultPageSettings.Margins = New Margins(0, 0, 0, 0)

		pstg.PrinterName = Config.GetSetting(Variables.PrinterKeyConfig)

		AddHandler pd.PrintPage, New PrintPageEventHandler(AddressOf PrintPage)

		If pstg.IsValid Then

			pd.PrinterSettings = pstg

			Try
				pd.Print()
			Catch ex As InvalidPrinterException
				MessageBox.Show(frm, "LA IMPRESORA NO ESTA DISPONIBLE, ES POSIBLE QUE ESTE AVERIADA O QUE NO SE ENCUENTRE CONECTADA." + vbCrLf + vbCrLf + "N° DE TICKET:" + Me.abrev + Me.corre, "ERROR EN LA IMPRESIÓN", MessageBoxIcon.Error, MessageBoxButtons.OK)
			End Try


		Else

			MessageBox.Show(frm, "LA IMPRESORA NO ESTA DISPONIBLE, ES POSIBLE QUE ESTE AVERIADA O QUE NO SE ENCUENTRE CONECTADA." + vbCrLf + vbCrLf + "N° DE TICKET:" + Me.abrev + Me.corre, "ERROR EN LA IMPRESIÓN", MessageBoxIcon.Error, MessageBoxButtons.OK)

		End If

	End Sub

End Class
