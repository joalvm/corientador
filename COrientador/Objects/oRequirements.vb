﻿Public Class oRequirements
	Public rowid As Integer
	Public local_id As Integer
	Public local As String
	Public requisito_id As Integer
	Public nombre As String
	Public titulo As String
	Public descripcion As String
	Public esta_habilitado As Boolean
	Public last_modification As Integer
	Public state As Boolean
	Public items As List(Of oRequirementsList)
End Class
