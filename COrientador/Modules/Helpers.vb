﻿Module Helpers

	Function isDomainAvailable(ByVal HostAddress As String) As Boolean

		Try

			If My.Computer.Network.IsAvailable AndAlso My.Computer.Network.Ping(HostAddress, 1000) Then
				Return True
			Else
				Return False
			End If

		Catch ex As Exception
			Return False
		End Try

	End Function

	Function isNetworkAvailable() As Boolean

		If My.Computer.Network.IsAvailable Then
			Return True
		Else
			Return False
		End If

	End Function

	Function getMAC() As String

		Dim mac As String = String.Empty
		Dim nics() As System.Net.NetworkInformation.NetworkInterface = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()

		For Each network In nics

			If network.OperationalStatus = Net.NetworkInformation.OperationalStatus.Up Then
				mac = network.GetPhysicalAddress.ToString
				Exit For
			End If

		Next

		Return mac

	End Function

	Function getPcName() As String

		Return System.Net.Dns.GetHostName

	End Function

End Module
