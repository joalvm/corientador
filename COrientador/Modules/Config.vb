﻿Imports System.Configuration
Imports MaterialSkin

Module Config

	Public baseUrl As String = (GetSetting(Variables.ProtocolKeyConfig) + GetSetting(Variables.WebServiceKeyConfig))

	Public Sub FormSkin(form As Form)

		Dim SkinManager As MaterialSkinManager = MaterialSkinManager.Instance

		SkinManager.AddFormToManage(form)
		SkinManager.Theme = MaterialSkinManager.Themes.LIGHT
		SkinManager.ColorScheme = New ColorScheme(Primary.Orange500, Primary.Orange700, Primary.DeepOrange900, Accent.Indigo400, TextShade.WHITE)

	End Sub

	Public Function GetSettings() As Dictionary(Of String, String)

		Dim dict As New Dictionary(Of String, String)

		Try

			Dim appSettings = ConfigurationManager.AppSettings

			If appSettings.Count = 0 Then
				Return dict.DefaultIfEmpty
			Else

				For Each key As String In appSettings.AllKeys
					dict.Add(key, appSettings(key))
				Next

				Return dict

			End If

			Return dict

		Catch e As ConfigurationErrorsException
			Return dict
		End Try

	End Function

	Public Function GetSetting(key As String) As String

		Try

			Dim appSettings = ConfigurationManager.AppSettings
			Dim result As String = appSettings(key)

			If IsNothing(result) Then
				result = String.Empty
			End If

			Return result

		Catch e As ConfigurationErrorsException
			Return String.Empty
		End Try

	End Function

	Public Sub UpdateSettings(key As String, value As String)

		Try

			Dim configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
			Dim settings = configFile.AppSettings.Settings

			If IsNothing(settings(key)) Then
				settings.Add(key, value)
			Else
				settings(key).Value = value
			End If

			configFile.Save(ConfigurationSaveMode.Modified)

			ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name)

		Catch e As ConfigurationErrorsException
			Console.WriteLine("Error writing app settings")
		End Try

	End Sub

End Module

