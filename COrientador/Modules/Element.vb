﻿Module Element

	Public Function ButtonOption(ByVal Text As String, ByVal Name As String) As MaterialSkin.Controls.MaterialRaisedButton

		Dim button As New MaterialSkin.Controls.MaterialRaisedButton

		button.AutoEllipsis = True
		button.Depth = 0
		button.Margin = New System.Windows.Forms.Padding(6)
		button.MaximumSize = New System.Drawing.Size(240, 56)
		button.MinimumSize = New System.Drawing.Size(240, 56)
		button.MouseState = MaterialSkin.MouseState.HOVER
		button.Name = Name
		button.Padding = New System.Windows.Forms.Padding(8)
		button.Primary = True
		button.Size = New System.Drawing.Size(240, 70)
		button.Text = Text
		button.UseCompatibleTextRendering = True
		button.UseVisualStyleBackColor = True

		Return button

	End Function

	Public Function LabelAction(ByVal Text As String, ByVal Name As String) As Label

		Dim melabel As New Label

		melabel.AutoSize = True
		melabel.Dock = System.Windows.Forms.DockStyle.Top
		melabel.Font = New System.Drawing.Font("Roboto", 11.0!)
		melabel.ForeColor = System.Drawing.Color.DimGray
		melabel.TextAlign = ContentAlignment.MiddleRight
		melabel.Name = Name
		melabel.Text = Text

		Return melabel


	End Function

	Public Sub setToolTip(ByRef ctrl As Control, ByVal content As String, ByVal title As String, ByVal type As ToolTipIcon)

		Dim ttp As New ToolTip

		ttp.ToolTipTitle = title
		ttp.ToolTipIcon = type
		ttp.AutomaticDelay = 300
		ttp.AutoPopDelay = 5000
		ttp.IsBalloon = True

		ttp.SetToolTip(ctrl, content)

	End Sub

End Module
