﻿Module Variables

#Region "Autorización de aplicativo"
	Public IdKeyConfig As String = "ID"
	Public CodeKeyConfig As String = "CODE"
	Public TokenKeyConfig As String = "AUTH"
#End Region

#Region "Sincronización"
	Public ProtocolKeyConfig As String = "PROTOCOL"
	Public WebServiceKeyConfig As String = "WS"
#End Region

#Region "Datos del local"
	Public LocalIdKeyConfig As String = "LOCAL_ID"
	Public LocalKeyConfig As String = "LOCAL_NAME"
	Public LocalAddressKeyConfig As String = "LOCAL_ADDRESS"
	Public LocalStationKeyConfig As String = "LOCAL_STATION"
	Public HoraStartKeyConfig As String = "H_START"
	Public HoraFinishKeyConfig As String = "H_FINISH"
#End Region

#Region "Componentes"
	Public ScannerKeyConfig As String = "SCANNER"
	Public PrinterKeyConfig As String = "PRINTER"
#End Region

#Region "Datos de Usuario"
	Public UserIdKeyConfig As String = "USER_ID"
	Public UserNameKeyConfig As String = "USER_NAME"
	Public UserLastNameKeyConfig As String = "USER_LASTNAME"
	Public UserRoleKeyConfig As String = "ROLE"
#End Region

#Region "Datos Seleccionados"
	Public SelectedServiceIdKeyConfig As String = "SERVICE_ID"
	Public SelectedServiceKeyConfig As String = "SERVICE_NAME"
	Public selectedPreferenceIdKeyConfig As String = "PREFERENCE_ID"
	Public SelectedPreferenceKeyConfig As String = "PREFERENCE_NAME"
	Public SelectedDNIKeyConfig As String = "DNI"
	Public SelectedClientKeyConfig As String = "CLIENT"
#End Region

#Region "Datos de la PC"
	Public PCNameKeyConfig As String = "PCNAME"
	Public MACKeyConfig As String = "MAC"
#End Region

#Region "Configuraciones del aplicativo"
	Public AskBeforePrinting As String = "ASKBEFOREPRINTING"
#End Region


End Module
