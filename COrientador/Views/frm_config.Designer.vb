﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_config
	Inherits MaterialSkin.Controls.MaterialForm

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.btn_cancel = New MaterialSkin.Controls.MaterialFlatButton()
		Me.btn_save = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.MaterialLabel4 = New MaterialSkin.Controls.MaterialLabel()
		Me.MaterialLabel3 = New MaterialSkin.Controls.MaterialLabel()
		Me.cbo_printers = New System.Windows.Forms.ComboBox()
		Me.cbo_scanners = New System.Windows.Forms.ComboBox()
		Me.cbo_protocols = New System.Windows.Forms.ComboBox()
		Me.MaterialLabel2 = New MaterialSkin.Controls.MaterialLabel()
		Me.txt_url = New MaterialSkin.Controls.MaterialSingleLineTextField()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.txt_idapp = New System.Windows.Forms.TextBox()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.btn_registerApp = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.MaterialLabel5 = New MaterialSkin.Controls.MaterialLabel()
		Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
		Me.lbl_horario = New MaterialSkin.Controls.MaterialLabel()
		Me.lbl_local = New MaterialSkin.Controls.MaterialLabel()
		Me.chkb_confirmprinting = New MaterialSkin.Controls.MaterialCheckBox()
		Me.GroupBox1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.SuspendLayout()
		'
		'btn_cancel
		'
		Me.btn_cancel.AutoSize = True
		Me.btn_cancel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.btn_cancel.Depth = 0
		Me.btn_cancel.Location = New System.Drawing.Point(203, 553)
		Me.btn_cancel.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
		Me.btn_cancel.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_cancel.Name = "btn_cancel"
		Me.btn_cancel.Primary = False
		Me.btn_cancel.Size = New System.Drawing.Size(82, 36)
		Me.btn_cancel.TabIndex = 9
		Me.btn_cancel.Text = "cancelar"
		Me.btn_cancel.UseVisualStyleBackColor = True
		'
		'btn_save
		'
		Me.btn_save.Depth = 0
		Me.btn_save.Location = New System.Drawing.Point(292, 553)
		Me.btn_save.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_save.Name = "btn_save"
		Me.btn_save.Primary = True
		Me.btn_save.Size = New System.Drawing.Size(86, 36)
		Me.btn_save.TabIndex = 8
		Me.btn_save.Text = "guardar"
		Me.btn_save.UseVisualStyleBackColor = True
		'
		'MaterialLabel4
		'
		Me.MaterialLabel4.AutoSize = True
		Me.MaterialLabel4.Depth = 0
		Me.MaterialLabel4.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel4.Location = New System.Drawing.Point(13, 88)
		Me.MaterialLabel4.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.MaterialLabel4.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel4.Name = "MaterialLabel4"
		Me.MaterialLabel4.Size = New System.Drawing.Size(77, 19)
		Me.MaterialLabel4.TabIndex = 0
		Me.MaterialLabel4.Text = "Impresora"
		'
		'MaterialLabel3
		'
		Me.MaterialLabel3.AutoSize = True
		Me.MaterialLabel3.Depth = 0
		Me.MaterialLabel3.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel3.Location = New System.Drawing.Point(13, 31)
		Me.MaterialLabel3.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.MaterialLabel3.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel3.Name = "MaterialLabel3"
		Me.MaterialLabel3.Size = New System.Drawing.Size(63, 19)
		Me.MaterialLabel3.TabIndex = 0
		Me.MaterialLabel3.Text = "Escaner"
		'
		'cbo_printers
		'
		Me.cbo_printers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cbo_printers.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cbo_printers.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
		Me.cbo_printers.FormattingEnabled = True
		Me.cbo_printers.Location = New System.Drawing.Point(13, 110)
		Me.cbo_printers.Name = "cbo_printers"
		Me.cbo_printers.Size = New System.Drawing.Size(319, 26)
		Me.cbo_printers.TabIndex = 6
		'
		'cbo_scanners
		'
		Me.cbo_scanners.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cbo_scanners.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cbo_scanners.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
		Me.cbo_scanners.FormattingEnabled = True
		Me.cbo_scanners.Location = New System.Drawing.Point(13, 53)
		Me.cbo_scanners.Name = "cbo_scanners"
		Me.cbo_scanners.Size = New System.Drawing.Size(319, 26)
		Me.cbo_scanners.TabIndex = 5
		'
		'cbo_protocols
		'
		Me.cbo_protocols.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cbo_protocols.FormattingEnabled = True
		Me.cbo_protocols.Items.AddRange(New Object() {"http://", "https://", "ftp://", "ftps://"})
		Me.cbo_protocols.Location = New System.Drawing.Point(17, 90)
		Me.cbo_protocols.Name = "cbo_protocols"
		Me.cbo_protocols.Size = New System.Drawing.Size(72, 26)
		Me.cbo_protocols.TabIndex = 2
		'
		'MaterialLabel2
		'
		Me.MaterialLabel2.AutoSize = True
		Me.MaterialLabel2.Depth = 0
		Me.MaterialLabel2.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel2.Location = New System.Drawing.Point(13, 29)
		Me.MaterialLabel2.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.MaterialLabel2.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel2.Name = "MaterialLabel2"
		Me.MaterialLabel2.Size = New System.Drawing.Size(53, 19)
		Me.MaterialLabel2.TabIndex = 0
		Me.MaterialLabel2.Text = "ID App"
		'
		'txt_url
		'
		Me.txt_url.Depth = 0
		Me.txt_url.Hint = "miwebservice.com"
		Me.txt_url.Location = New System.Drawing.Point(95, 93)
		Me.txt_url.MouseState = MaterialSkin.MouseState.HOVER
		Me.txt_url.Name = "txt_url"
		Me.txt_url.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
		Me.txt_url.SelectedText = ""
		Me.txt_url.SelectionLength = 0
		Me.txt_url.SelectionStart = 0
		Me.txt_url.Size = New System.Drawing.Size(237, 23)
		Me.txt_url.TabIndex = 3
		Me.txt_url.UseSystemPasswordChar = False
		'
		'GroupBox1
		'
		Me.GroupBox1.AutoSize = True
		Me.GroupBox1.Controls.Add(Me.chkb_confirmprinting)
		Me.GroupBox1.Controls.Add(Me.MaterialLabel3)
		Me.GroupBox1.Controls.Add(Me.cbo_scanners)
		Me.GroupBox1.Controls.Add(Me.cbo_printers)
		Me.GroupBox1.Controls.Add(Me.MaterialLabel4)
		Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.GroupBox1.Font = New System.Drawing.Font("Roboto", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GroupBox1.ForeColor = System.Drawing.Color.Gray
		Me.GroupBox1.Location = New System.Drawing.Point(19, 328)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Padding = New System.Windows.Forms.Padding(24, 0, 24, 0)
		Me.GroupBox1.Size = New System.Drawing.Size(359, 198)
		Me.GroupBox1.TabIndex = 7
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Componentes"
		'
		'txt_idapp
		'
		Me.txt_idapp.Location = New System.Drawing.Point(17, 51)
		Me.txt_idapp.Name = "txt_idapp"
		Me.txt_idapp.Size = New System.Drawing.Size(315, 27)
		Me.txt_idapp.TabIndex = 1
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.btn_registerApp)
		Me.GroupBox2.Controls.Add(Me.MaterialLabel5)
		Me.GroupBox2.Controls.Add(Me.MaterialLabel1)
		Me.GroupBox2.Controls.Add(Me.lbl_horario)
		Me.GroupBox2.Controls.Add(Me.lbl_local)
		Me.GroupBox2.Controls.Add(Me.txt_url)
		Me.GroupBox2.Controls.Add(Me.txt_idapp)
		Me.GroupBox2.Controls.Add(Me.cbo_protocols)
		Me.GroupBox2.Controls.Add(Me.MaterialLabel2)
		Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.GroupBox2.Font = New System.Drawing.Font("Roboto", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GroupBox2.ForeColor = System.Drawing.Color.Gray
		Me.GroupBox2.Location = New System.Drawing.Point(19, 84)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(24, 0, 24, 0)
		Me.GroupBox2.Size = New System.Drawing.Size(355, 238)
		Me.GroupBox2.TabIndex = 8
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Registrar Aplicación (*)"
		'
		'btn_registerApp
		'
		Me.btn_registerApp.Depth = 0
		Me.btn_registerApp.Location = New System.Drawing.Point(233, 178)
		Me.btn_registerApp.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_registerApp.Name = "btn_registerApp"
		Me.btn_registerApp.Primary = True
		Me.btn_registerApp.Size = New System.Drawing.Size(99, 37)
		Me.btn_registerApp.TabIndex = 4
		Me.btn_registerApp.Text = "REGISTRAR"
		Me.btn_registerApp.UseVisualStyleBackColor = True
		'
		'MaterialLabel5
		'
		Me.MaterialLabel5.Depth = 0
		Me.MaterialLabel5.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel5.Location = New System.Drawing.Point(21, 156)
		Me.MaterialLabel5.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.MaterialLabel5.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel5.Name = "MaterialLabel5"
		Me.MaterialLabel5.Size = New System.Drawing.Size(79, 19)
		Me.MaterialLabel5.TabIndex = 12
		Me.MaterialLabel5.Text = "Dirección: "
		Me.MaterialLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'MaterialLabel1
		'
		Me.MaterialLabel1.Depth = 0
		Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel1.Location = New System.Drawing.Point(27, 130)
		Me.MaterialLabel1.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel1.Name = "MaterialLabel1"
		Me.MaterialLabel1.Size = New System.Drawing.Size(72, 19)
		Me.MaterialLabel1.TabIndex = 11
		Me.MaterialLabel1.Text = "Local:"
		Me.MaterialLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'lbl_horario
		'
		Me.lbl_horario.Depth = 0
		Me.lbl_horario.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_horario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lbl_horario.Location = New System.Drawing.Point(109, 156)
		Me.lbl_horario.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.lbl_horario.MouseState = MaterialSkin.MouseState.HOVER
		Me.lbl_horario.Name = "lbl_horario"
		Me.lbl_horario.Size = New System.Drawing.Size(219, 19)
		Me.lbl_horario.TabIndex = 9
		'
		'lbl_local
		'
		Me.lbl_local.Depth = 0
		Me.lbl_local.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_local.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lbl_local.Location = New System.Drawing.Point(109, 130)
		Me.lbl_local.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
		Me.lbl_local.MouseState = MaterialSkin.MouseState.HOVER
		Me.lbl_local.Name = "lbl_local"
		Me.lbl_local.Size = New System.Drawing.Size(219, 19)
		Me.lbl_local.TabIndex = 9
		'
		'chkb_confirmprinting
		'
		Me.chkb_confirmprinting.AutoSize = True
		Me.chkb_confirmprinting.Depth = 0
		Me.chkb_confirmprinting.Font = New System.Drawing.Font("Roboto", 10.0!)
		Me.chkb_confirmprinting.Location = New System.Drawing.Point(13, 148)
		Me.chkb_confirmprinting.Margin = New System.Windows.Forms.Padding(0)
		Me.chkb_confirmprinting.MouseLocation = New System.Drawing.Point(-1, -1)
		Me.chkb_confirmprinting.MouseState = MaterialSkin.MouseState.HOVER
		Me.chkb_confirmprinting.Name = "chkb_confirmprinting"
		Me.chkb_confirmprinting.Ripple = True
		Me.chkb_confirmprinting.Size = New System.Drawing.Size(157, 30)
		Me.chkb_confirmprinting.TabIndex = 7
		Me.chkb_confirmprinting.Text = "Confirmar Impresión"
		Me.chkb_confirmprinting.UseVisualStyleBackColor = True
		'
		'frm_config
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 26.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(398, 600)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.btn_cancel)
		Me.Controls.Add(Me.btn_save)
		Me.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(6)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frm_config"
		Me.Padding = New System.Windows.Forms.Padding(8, 76, 8, 8)
		Me.ShowIcon = False
		Me.ShowInTaskbar = False
		Me.Sizable = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "Configuraciones"
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents btn_cancel As MaterialSkin.Controls.MaterialFlatButton
	Friend WithEvents btn_save As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents MaterialLabel4 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel3 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents cbo_printers As ComboBox
	Friend WithEvents cbo_scanners As ComboBox
	Friend WithEvents cbo_protocols As ComboBox
	Friend WithEvents MaterialLabel2 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents txt_url As MaterialSkin.Controls.MaterialSingleLineTextField
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents txt_idapp As TextBox
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents lbl_local As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents lbl_horario As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel5 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents btn_registerApp As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents chkb_confirmprinting As MaterialSkin.Controls.MaterialCheckBox
End Class
