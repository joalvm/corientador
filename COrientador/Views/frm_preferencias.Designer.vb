﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_preferencias
	Inherits MaterialSkin.Controls.MaterialForm

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.flowPanelPreferences = New System.Windows.Forms.FlowLayoutPanel()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.lbl_cliente = New System.Windows.Forms.Label()
		Me.lbl_servicio = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.btn_back = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.btn_clean = New MaterialSkin.Controls.MaterialFlatButton()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.lbl_user = New System.Windows.Forms.Label()
		Me.lbl_role = New System.Windows.Forms.Label()
		Me.PictureBox1 = New System.Windows.Forms.PictureBox()
		Me.lbl_local = New System.Windows.Forms.Label()
		Me.Panel1.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.Panel2.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.flowPanelPreferences)
		Me.Panel1.Controls.Add(Me.Panel3)
		Me.Panel1.Controls.Add(Me.btn_back)
		Me.Panel1.Controls.Add(Me.btn_clean)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel1.Location = New System.Drawing.Point(0, 64)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Padding = New System.Windows.Forms.Padding(12)
		Me.Panel1.Size = New System.Drawing.Size(920, 576)
		Me.Panel1.TabIndex = 0
		'
		'flowPanelPreferences
		'
		Me.flowPanelPreferences.Dock = System.Windows.Forms.DockStyle.Top
		Me.flowPanelPreferences.Location = New System.Drawing.Point(12, 68)
		Me.flowPanelPreferences.Margin = New System.Windows.Forms.Padding(12)
		Me.flowPanelPreferences.Name = "flowPanelPreferences"
		Me.flowPanelPreferences.Padding = New System.Windows.Forms.Padding(12, 24, 12, 12)
		Me.flowPanelPreferences.Size = New System.Drawing.Size(896, 432)
		Me.flowPanelPreferences.TabIndex = 17
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.lbl_cliente)
		Me.Panel3.Controls.Add(Me.lbl_servicio)
		Me.Panel3.Controls.Add(Me.Label2)
		Me.Panel3.Controls.Add(Me.Label1)
		Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel3.Location = New System.Drawing.Point(12, 12)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(896, 56)
		Me.Panel3.TabIndex = 16
		'
		'lbl_cliente
		'
		Me.lbl_cliente.Font = New System.Drawing.Font("Roboto", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_cliente.ForeColor = System.Drawing.SystemColors.GrayText
		Me.lbl_cliente.Location = New System.Drawing.Point(83, 7)
		Me.lbl_cliente.Name = "lbl_cliente"
		Me.lbl_cliente.Size = New System.Drawing.Size(478, 19)
		Me.lbl_cliente.TabIndex = 1
		'
		'lbl_servicio
		'
		Me.lbl_servicio.Font = New System.Drawing.Font("Roboto", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_servicio.ForeColor = System.Drawing.SystemColors.GrayText
		Me.lbl_servicio.Location = New System.Drawing.Point(83, 31)
		Me.lbl_servicio.Name = "lbl_servicio"
		Me.lbl_servicio.Size = New System.Drawing.Size(478, 19)
		Me.lbl_servicio.TabIndex = 1
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
		Me.Label2.Location = New System.Drawing.Point(8, 31)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(71, 19)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "Servicio: "
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
		Me.Label1.Location = New System.Drawing.Point(8, 7)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(60, 19)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "Cliente:"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'btn_back
		'
		Me.btn_back.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btn_back.Depth = 0
		Me.btn_back.Location = New System.Drawing.Point(769, 510)
		Me.btn_back.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_back.Name = "btn_back"
		Me.btn_back.Primary = True
		Me.btn_back.Size = New System.Drawing.Size(136, 48)
		Me.btn_back.TabIndex = 15
		Me.btn_back.Text = "atras"
		Me.btn_back.UseVisualStyleBackColor = True
		'
		'btn_clean
		'
		Me.btn_clean.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btn_clean.AutoSize = True
		Me.btn_clean.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.btn_clean.Depth = 0
		Me.btn_clean.Location = New System.Drawing.Point(24, 516)
		Me.btn_clean.Margin = New System.Windows.Forms.Padding(24, 12, 24, 12)
		Me.btn_clean.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_clean.Name = "btn_clean"
		Me.btn_clean.Primary = False
		Me.btn_clean.Size = New System.Drawing.Size(76, 36)
		Me.btn_clean.TabIndex = 11
		Me.btn_clean.Text = "Reiniciar"
		Me.btn_clean.UseVisualStyleBackColor = True
		'
		'Panel2
		'
		Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel2.BackColor = System.Drawing.Color.Transparent
		Me.Panel2.Controls.Add(Me.lbl_user)
		Me.Panel2.Controls.Add(Me.lbl_role)
		Me.Panel2.Controls.Add(Me.PictureBox1)
		Me.Panel2.Location = New System.Drawing.Point(571, 24)
		Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Padding = New System.Windows.Forms.Padding(2)
		Me.Panel2.Size = New System.Drawing.Size(337, 40)
		Me.Panel2.TabIndex = 2
		'
		'lbl_user
		'
		Me.lbl_user.Dock = System.Windows.Forms.DockStyle.Top
		Me.lbl_user.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_user.ForeColor = System.Drawing.Color.White
		Me.lbl_user.Location = New System.Drawing.Point(2, 2)
		Me.lbl_user.Margin = New System.Windows.Forms.Padding(0)
		Me.lbl_user.Name = "lbl_user"
		Me.lbl_user.Size = New System.Drawing.Size(298, 20)
		Me.lbl_user.TabIndex = 1
		Me.lbl_user.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'lbl_role
		'
		Me.lbl_role.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.lbl_role.Font = New System.Drawing.Font("Roboto", 9.0!)
		Me.lbl_role.ForeColor = System.Drawing.Color.White
		Me.lbl_role.Location = New System.Drawing.Point(2, 20)
		Me.lbl_role.Margin = New System.Windows.Forms.Padding(0)
		Me.lbl_role.Name = "lbl_role"
		Me.lbl_role.Size = New System.Drawing.Size(298, 18)
		Me.lbl_role.TabIndex = 2
		Me.lbl_role.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'PictureBox1
		'
		Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Right
		Me.PictureBox1.Image = Global.COrientador.My.Resources.Resources.icon_material_action_perm_identity
		Me.PictureBox1.Location = New System.Drawing.Point(300, 2)
		Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Size = New System.Drawing.Size(35, 36)
		Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
		Me.PictureBox1.TabIndex = 0
		Me.PictureBox1.TabStop = False
		'
		'lbl_local
		'
		Me.lbl_local.BackColor = System.Drawing.Color.Transparent
		Me.lbl_local.Font = New System.Drawing.Font("Roboto", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_local.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.lbl_local.Location = New System.Drawing.Point(14, 5)
		Me.lbl_local.Name = "lbl_local"
		Me.lbl_local.Size = New System.Drawing.Size(289, 15)
		Me.lbl_local.TabIndex = 3
		'
		'frm_preferencias
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 26.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(920, 640)
		Me.Controls.Add(Me.lbl_local)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Panel1)
		Me.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(6)
		Me.MinimumSize = New System.Drawing.Size(920, 640)
		Me.Name = "frm_preferencias"
		Me.Padding = New System.Windows.Forms.Padding(0, 64, 0, 0)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Preferencias"
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.Panel3.ResumeLayout(False)
		Me.Panel3.PerformLayout()
		Me.Panel2.ResumeLayout(False)
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Panel1 As Panel
	Friend WithEvents btn_back As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents btn_clean As MaterialSkin.Controls.MaterialFlatButton
	Friend WithEvents Panel2 As Panel
	Friend WithEvents lbl_user As Label
	Friend WithEvents lbl_role As Label
	Friend WithEvents PictureBox1 As PictureBox
	Friend WithEvents lbl_local As Label
	Friend WithEvents flowPanelPreferences As FlowLayoutPanel
	Friend WithEvents Panel3 As Panel
	Friend WithEvents lbl_cliente As Label
	Friend WithEvents lbl_servicio As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label1 As Label
End Class
