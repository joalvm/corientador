﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_login
	Inherits MaterialSkin.Controls.MaterialForm

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.divMain = New System.Windows.Forms.Panel()
		Me.PictureBox1 = New System.Windows.Forms.PictureBox()
		Me.btn_send = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.txt_password = New MaterialSkin.Controls.MaterialSingleLineTextField()
		Me.txt_user = New MaterialSkin.Controls.MaterialSingleLineTextField()
		Me.btn_config = New System.Windows.Forms.PictureBox()
		Me.lbl_local = New System.Windows.Forms.Label()
		Me.divMain.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.btn_config, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
		Me.Label1.Font = New System.Drawing.Font("Roboto Light", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.Gray
		Me.Label1.Location = New System.Drawing.Point(346, 595)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(223, 26)
		Me.Label1.TabIndex = 6
		Me.Label1.Text = "Sistema de Gestión de Colas." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2016 © Municipalidad Provincial del Callao"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'divMain
		'
		Me.divMain.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.divMain.Controls.Add(Me.PictureBox1)
		Me.divMain.Controls.Add(Me.btn_send)
		Me.divMain.Controls.Add(Me.txt_password)
		Me.divMain.Controls.Add(Me.txt_user)
		Me.divMain.Enabled = False
		Me.divMain.Location = New System.Drawing.Point(299, 106)
		Me.divMain.Name = "divMain"
		Me.divMain.Size = New System.Drawing.Size(323, 345)
		Me.divMain.TabIndex = 5
		'
		'PictureBox1
		'
		Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.PictureBox1.Image = Global.COrientador.My.Resources.Resources.logo_desc
		Me.PictureBox1.Location = New System.Drawing.Point(50, 26)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Size = New System.Drawing.Size(220, 80)
		Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
		Me.PictureBox1.TabIndex = 3
		Me.PictureBox1.TabStop = False
		'
		'btn_send
		'
		Me.btn_send.Depth = 0
		Me.btn_send.Location = New System.Drawing.Point(82, 269)
		Me.btn_send.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_send.Name = "btn_send"
		Me.btn_send.Primary = True
		Me.btn_send.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.btn_send.Size = New System.Drawing.Size(149, 48)
		Me.btn_send.TabIndex = 3
		Me.btn_send.Text = "Ingresar"
		Me.btn_send.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btn_send.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.btn_send.UseVisualStyleBackColor = False
		'
		'txt_password
		'
		Me.txt_password.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
		Me.txt_password.Depth = 0
		Me.txt_password.Font = New System.Drawing.Font("Roboto", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txt_password.Hint = "Contraseña"
		Me.txt_password.Location = New System.Drawing.Point(50, 212)
		Me.txt_password.MouseState = MaterialSkin.MouseState.HOVER
		Me.txt_password.Name = "txt_password"
		Me.txt_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
		Me.txt_password.SelectedText = ""
		Me.txt_password.SelectionLength = 0
		Me.txt_password.SelectionStart = 0
		Me.txt_password.Size = New System.Drawing.Size(220, 23)
		Me.txt_password.TabIndex = 2
		Me.txt_password.UseSystemPasswordChar = True
		'
		'txt_user
		'
		Me.txt_user.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
		Me.txt_user.Depth = 0
		Me.txt_user.Font = New System.Drawing.Font("Roboto", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txt_user.Hint = "Usuario"
		Me.txt_user.Location = New System.Drawing.Point(50, 161)
		Me.txt_user.MouseState = MaterialSkin.MouseState.HOVER
		Me.txt_user.Name = "txt_user"
		Me.txt_user.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
		Me.txt_user.SelectedText = ""
		Me.txt_user.SelectionLength = 0
		Me.txt_user.SelectionStart = 0
		Me.txt_user.Size = New System.Drawing.Size(220, 23)
		Me.txt_user.TabIndex = 1
		Me.txt_user.UseSystemPasswordChar = False
		'
		'btn_config
		'
		Me.btn_config.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btn_config.Cursor = System.Windows.Forms.Cursors.Hand
		Me.btn_config.Image = Global.COrientador.My.Resources.Resources.icon_material_action_settings
		Me.btn_config.Location = New System.Drawing.Point(884, 70)
		Me.btn_config.Name = "btn_config"
		Me.btn_config.Size = New System.Drawing.Size(24, 24)
		Me.btn_config.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
		Me.btn_config.TabIndex = 7
		Me.btn_config.TabStop = False
		'
		'lbl_local
		'
		Me.lbl_local.BackColor = System.Drawing.Color.Transparent
		Me.lbl_local.Font = New System.Drawing.Font("Roboto", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_local.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.lbl_local.Location = New System.Drawing.Point(14, 5)
		Me.lbl_local.Name = "lbl_local"
		Me.lbl_local.Size = New System.Drawing.Size(289, 15)
		Me.lbl_local.TabIndex = 8
		'
		'frm_login
		'
		Me.AcceptButton = Me.btn_send
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 26.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(920, 640)
		Me.Controls.Add(Me.lbl_local)
		Me.Controls.Add(Me.btn_config)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.divMain)
		Me.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(6)
		Me.MinimumSize = New System.Drawing.Size(920, 640)
		Me.Name = "frm_login"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "LOGIN"
		Me.divMain.ResumeLayout(False)
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.btn_config, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents divMain As Panel
	Friend WithEvents PictureBox1 As PictureBox
	Friend WithEvents btn_send As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents txt_password As MaterialSkin.Controls.MaterialSingleLineTextField
	Friend WithEvents btn_config As PictureBox
	Private WithEvents txt_user As MaterialSkin.Controls.MaterialSingleLineTextField
	Friend WithEvents lbl_local As Label
End Class
