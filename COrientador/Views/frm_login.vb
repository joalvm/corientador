﻿Imports System.Collections.Specialized
Imports MaterialSkin
Imports System.Threading
Imports RestSharp
Imports Newtonsoft.Json
Imports System.Net.NetworkInformation

Public Class frm_login

	Private Sub frm_login_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Config.FormSkin(Me)

		Dim ws = Config.GetSetting(Variables.WebServiceKeyConfig)
		Dim id = Config.GetSetting(Variables.IdKeyConfig)
		Dim token = Config.GetSetting(Variables.TokenKeyConfig)

		Me.divMain.Enabled = False

		If (ws.Length > 0 And id.Length > 0 And token.Length > 0) Then

			Dim QSync As New Sync(Me)

			QSync.Init(New oParamsSync With {
				.type = Method.GET,
				.resource = "sync/init",
				.format = DataFormat.Json,
				.data = Nothing,
				.DataDeserialize = New oInitApp(),
				.Success = AddressOf CheckInitialSettings
			})

		Else
			MessageBox.Show(Me, "No se han establecido datos de conexión", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End If

	End Sub

	Private Sub btn_send_Click(sender As Object, e As EventArgs) Handles btn_send.Click

		If Me.txt_user.Text.Trim().Length < 5 Then
			MessageBox.Show("El campo de usuario es obligatorio y la cantidad de caracteres debe ser mayor a 5")
			Return
		End If

		If Me.txt_password.Text.Trim().Length < 6 Then
			MessageBox.Show("El campo de usuario es obligatorio y la cantidad de caracteres debe ser mayor o igual a 6")
			Return
		End If

		Dim QSync As New Sync(Me)

		Dim data As New StringDictionary

		data.Add("user", Me.txt_user.Text.Trim())
		data.Add("pass", Me.txt_password.Text)

		QSync.Init(New oParamsSync With {
			.type = Method.POST,
			.resource = "sync/login",
			.format = DataFormat.Json,
			.data = data,
			.DataDeserialize = New oLogin,
			.Success = AddressOf ValidateUserLogin
		})

	End Sub

	Private Sub btn_config_Click(sender As Object, e As EventArgs) Handles btn_config.Click

		Dim config As New frm_config

		config.Owner = Me
		config.meParen = Me

		config.ShowDialog()

	End Sub


#Region "Valida la aplicación y obtiene los datos del local"

	Public Sub CheckInitialSettings(response As oResponse)

		If Not response.error Then

			Dim app As oInitApp = response.data
			Dim horario As List(Of oSchedule) = app.schedule
			Dim current_time As Date = app.server_datetime

			Dim indexHorario As Integer = app.schedule.FindIndex(Function(x) x.day = current_time.DayOfWeek)

			'VALIDAR SI EL DIA ACTUAL SE ENCUENTRA DENTRO DEL HORARIO DE LA APLICACIÓN VINCULADA AL LOCAL
			If indexHorario >= 0 Then

				Config.UpdateSettings(Variables.LocalIdKeyConfig, app.local_id)
				Config.UpdateSettings(Variables.LocalKeyConfig, app.local)
				Config.UpdateSettings(Variables.LocalAddressKeyConfig, app.direccion)
				Config.UpdateSettings(Variables.LocalStationKeyConfig, app.puesto)

				Config.UpdateSettings(Variables.ScannerKeyConfig, app.scanner)
				Config.UpdateSettings(Variables.PrinterKeyConfig, app.printer)

				Dim start_time = DateTime.Parse(app.schedule(indexHorario).hour_start)
				Dim finish_time = DateTime.Parse(app.schedule(indexHorario).hour_finish)

				'VALIDAR SI LA HORA ACTUAL ESTA DENTRO DEL HORARIO ASIGNADO AL LOCAL
				If current_time.TimeOfDay.Ticks >= start_time.TimeOfDay.Ticks And current_time.TimeOfDay.Ticks <= finish_time.TimeOfDay.Ticks Then

					Config.UpdateSettings(Variables.HoraStartKeyConfig, start_time)
					Config.UpdateSettings(Variables.HoraFinishKeyConfig, finish_time)

					Me.lbl_local.Text = app.local + " - " + app.direccion
					Me.divMain.Enabled = True

					Me.txt_user.Focus()

				Else

					Me.divMain.Enabled = False
					MessageBox.Show(Me, "LA APLICACIÓN ESTARÁ ACTIVA SOLO EN LAS HORAS DE ATENCIÓN AL CLIENTE, CONFIGURADAS POR EL ADMINISTRADOR DEL SISTEMA", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

				End If

			Else

				Me.divMain.Enabled = False
				MessageBox.Show(Me, "LA APLICACIÓN ESTARÁ ACTIVA SOLO EN LOS DÍAS DE ATENCIÓN AL CLIENTE, CONFIGURADAS POR EL ADMINISTRADOR DEL SISTEMA", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

			End If

		Else

			Me.divMain.Enabled = False
			MessageBox.Show(Me, response.message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

		End If

	End Sub

	Public Sub ValidateUserLogin(response As oResponse)

		If Not response.error Then

			Dim lg As oLogin = response.data

			Config.UpdateSettings(Variables.UserIdKeyConfig, lg.id)
			Config.UpdateSettings(Variables.UserNameKeyConfig, lg.name)
			Config.UpdateSettings(Variables.UserLastNameKeyConfig, lg.lastName)
			Config.UpdateSettings(Variables.UserRoleKeyConfig, lg.role)

			MessageBox.Show("Bienvenido: " + lg.name + " " + lg.lastName)
			Dim frm As New frm_inicio

			frm.Show()
			Me.Close()

		Else

			MessageBox.Show(response.message)
			Me.txt_user.Focus()

		End If

	End Sub

#End Region

	Public Sub conexionPermitida(ByVal confim As Boolean)

		If confim Then

			Dim ws = Config.GetSetting(Variables.WebServiceKeyConfig)
			Dim id = Config.GetSetting(Variables.IdKeyConfig)
			Dim token = Config.GetSetting(Variables.TokenKeyConfig)

			Me.divMain.Enabled = False

			If (ws.Length > 0 And id.Length > 0 And token.Length > 0) Then

				Dim QSync As New Sync(Me)

				QSync.Init(New oParamsSync With {
					.type = Method.GET,
					.resource = "sync/init",
					.format = DataFormat.Json,
					.data = Nothing,
					.DataDeserialize = New oInitApp(),
					.Success = AddressOf CheckInitialSettings
				})

			Else
				MessageBox.Show(Me, "No se han establecido datos de conexión", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)
			End If

		End If

	End Sub

End Class