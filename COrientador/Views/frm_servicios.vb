﻿Imports MaterialSkin.Controls
Imports RestSharp

Public Class frm_servicios

	Private Sub frm_servicios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Config.FormSkin(Me)

		Me.lbl_local.Text = Config.GetSetting(Variables.LocalKeyConfig) + " - " + Config.GetSetting(Variables.LocalAddressKeyConfig)
		Me.lbl_user.Text = Config.GetSetting(Variables.UserNameKeyConfig) + " " + Config.GetSetting(Variables.UserLastNameKeyConfig)
		Me.lbl_role.Text = Config.GetSetting(Variables.UserRoleKeyConfig)

		Me.lbl_cliente.Text = Config.GetSetting(Variables.SelectedClientKeyConfig) + " | DNI: " + Config.GetSetting(Variables.SelectedDNIKeyConfig)

		Me.flowPanelGroups.Controls.Add(Element.LabelAction("Cargando Opciones...", "lbl_loading_01"))

		Dim QSync As New Sync(Me)

		QSync.Init(New oParamsSync With {
			.type = Method.GET,
			.resource = "options/services",
			.format = DataFormat.Json,
			.data = Nothing,
			.DataDeserialize = New List(Of oGroupServices),
			.Success = AddressOf loadServices
		})


	End Sub

	Private Sub loadServices(response As oResponse)

		If Not response.error Then

			Dim data As List(Of oGroupServices) = response.data
			Dim count As Integer = 1

			If Not data Is Nothing And data.Count > 0 Then

				Me.flowPanelGroups.Controls.RemoveAt(0)

				For Each row As oGroupServices In data

					Dim text As String = row.grupo
					Dim name As String = "btn_grpservice_" + count.ToString()

					Dim btn As MaterialRaisedButton = Element.ButtonOption(text, name)

					AddHandler btn.Click, AddressOf btn_group_Click

					btn.Tag = row

					Element.setToolTip(btn, row.descripcion, "Más información", ToolTipIcon.Info)

					Me.flowPanelGroups.SetFlowBreak(btn, True)

					Me.flowPanelGroups.Controls.Add(btn)

					count += 1

				Next

			End If

		Else

			MessageBox.Show(Me, response.message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

		End If


	End Sub

	Private Sub btn_group_Click(sender As Object, e As EventArgs)

		Dim btn As MaterialRaisedButton = sender
		Dim objServices As oGroupServices = btn.Tag
		Dim count As Integer = 1

		Me.flowPanelServices.Controls.Clear()

		If (objServices.servicios Is Nothing Or objServices.servicios.Count = 0) Then
			Me.flowPanelServices.Controls.Add(Element.LabelAction("La Opción no tiene servicios.", "lbl_notservices_01"))
			Return
		End If

		If (objServices.servicios.Count > 0) Then

			Dim lbl As Label = Element.LabelAction(objServices.grupo.ToUpperInvariant(), "lbl_notservices_02")

			Me.flowPanelGroups.SetFlowBreak(lbl, True)

			lbl.AutoSize = False
			lbl.Width = Me.flowPanelServices.Width - 24
			lbl.Margin = New Padding(0, 0, 0, 12)
			lbl.TextAlign = ContentAlignment.TopLeft

			Me.flowPanelServices.Controls.Add(lbl)

			For Each row As oServices In objServices.servicios

				Dim text As String = row.servicio
				Dim name As String = "btn_service_" + count.ToString()

				Dim boton As MaterialRaisedButton = Element.ButtonOption(text, name)

				AddHandler boton.Click, AddressOf btn_service_Click

				boton.Tag = row
				boton.MaximumSize = New System.Drawing.Size(240, 70)
				boton.MinimumSize = New System.Drawing.Size(240, 70)
				boton.Padding = New System.Windows.Forms.Padding(12)

				Element.setToolTip(btn, row.observacion, "Más información", ToolTipIcon.Info)

				Me.flowPanelServices.Controls.Add(boton)

				count += 1

			Next

		End If


	End Sub

	Private Sub btn_service_Click(sender As Object, e As EventArgs)

		Dim btn As MaterialRaisedButton = sender
		Dim objServices As oServices = btn.Tag

		Config.UpdateSettings(Variables.SelectedServiceKeyConfig, objServices.servicio)
		Config.UpdateSettings(Variables.SelectedServiceIdKeyConfig, objServices.rowid)

		Dim preferences As New frm_preferencias

		preferences.WindowState = Me.WindowState
		preferences.Show()

		Me.Close()

	End Sub

	Private Sub btn_clean_Click(sender As Object, e As EventArgs) Handles btn_clean.Click

		Dim frm As New frm_inicio

		Config.UpdateSettings(Variables.SelectedClientKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedDNIKeyConfig, String.Empty)

		frm.WindowState = Me.WindowState
		frm.Show()

		Me.Close()

	End Sub

End Class