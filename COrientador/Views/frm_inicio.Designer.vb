﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_inicio
	Inherits MaterialSkin.Controls.MaterialForm

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.ms_selector = New MaterialSkin.Controls.MaterialTabSelector()
		Me.tabControl = New MaterialSkin.Controls.MaterialTabControl()
		Me.tabAtencion = New System.Windows.Forms.TabPage()
		Me.lbl_loading = New System.Windows.Forms.Label()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.txtsearch = New System.Windows.Forms.TextBox()
		Me.flowPanelButtons = New System.Windows.Forms.FlowLayoutPanel()
		Me.btn_num07 = New System.Windows.Forms.Button()
		Me.btn_num08 = New System.Windows.Forms.Button()
		Me.btn_num09 = New System.Windows.Forms.Button()
		Me.btn_num04 = New System.Windows.Forms.Button()
		Me.btn_num05 = New System.Windows.Forms.Button()
		Me.btn_num06 = New System.Windows.Forms.Button()
		Me.btn_num01 = New System.Windows.Forms.Button()
		Me.btn_num02 = New System.Windows.Forms.Button()
		Me.btn_num03 = New System.Windows.Forms.Button()
		Me.btn_num00 = New System.Windows.Forms.Button()
		Me.btn_delete = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.btn_done = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.lbl_clientLastName = New MaterialSkin.Controls.MaterialLabel()
		Me.lbl_clientName = New MaterialSkin.Controls.MaterialLabel()
		Me.MaterialLabel8 = New MaterialSkin.Controls.MaterialLabel()
		Me.MaterialLabel9 = New MaterialSkin.Controls.MaterialLabel()
		Me.MaterialLabel10 = New MaterialSkin.Controls.MaterialLabel()
		Me.btn_AnonymousCustomer = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.btn_clean = New MaterialSkin.Controls.MaterialFlatButton()
		Me.TabRequisitos = New System.Windows.Forms.TabPage()
		Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.flowPanelRequirements = New System.Windows.Forms.FlowLayoutPanel()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.btn_printer = New MaterialSkin.Controls.MaterialRaisedButton()
		Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
		Me.tablist = New System.Windows.Forms.TabPage()
		Me.dgv_tickets = New System.Windows.Forms.DataGridView()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.lbl_user = New System.Windows.Forms.Label()
		Me.lbl_role = New System.Windows.Forms.Label()
		Me.PictureBox1 = New System.Windows.Forms.PictureBox()
		Me.lbl_local = New System.Windows.Forms.Label()
		Me.tabControl.SuspendLayout()
		Me.tabAtencion.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.flowPanelButtons.SuspendLayout()
		Me.TabRequisitos.SuspendLayout()
		Me.TableLayoutPanel1.SuspendLayout()
		Me.Panel2.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.tablist.SuspendLayout()
		CType(Me.dgv_tickets, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel4.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'ms_selector
		'
		Me.ms_selector.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
						Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ms_selector.BaseTabControl = Me.tabControl
		Me.ms_selector.Depth = 0
		Me.ms_selector.Location = New System.Drawing.Point(0, 64)
		Me.ms_selector.MouseState = MaterialSkin.MouseState.HOVER
		Me.ms_selector.Name = "ms_selector"
		Me.ms_selector.Size = New System.Drawing.Size(920, 57)
		Me.ms_selector.TabIndex = 0
		Me.ms_selector.Text = "MaterialTabSelector1"
		'
		'tabControl
		'
		Me.tabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
						Or System.Windows.Forms.AnchorStyles.Left) _
						Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.tabControl.Controls.Add(Me.tabAtencion)
		Me.tabControl.Controls.Add(Me.TabRequisitos)
		Me.tabControl.Controls.Add(Me.tablist)
		Me.tabControl.Depth = 0
		Me.tabControl.Location = New System.Drawing.Point(0, 127)
		Me.tabControl.MouseState = MaterialSkin.MouseState.HOVER
		Me.tabControl.Name = "tabControl"
		Me.tabControl.SelectedIndex = 0
		Me.tabControl.Size = New System.Drawing.Size(920, 513)
		Me.tabControl.TabIndex = 1
		'
		'tabAtencion
		'
		Me.tabAtencion.Controls.Add(Me.lbl_loading)
		Me.tabAtencion.Controls.Add(Me.Panel1)
		Me.tabAtencion.Controls.Add(Me.flowPanelButtons)
		Me.tabAtencion.Controls.Add(Me.lbl_clientLastName)
		Me.tabAtencion.Controls.Add(Me.lbl_clientName)
		Me.tabAtencion.Controls.Add(Me.MaterialLabel8)
		Me.tabAtencion.Controls.Add(Me.MaterialLabel9)
		Me.tabAtencion.Controls.Add(Me.MaterialLabel10)
		Me.tabAtencion.Controls.Add(Me.btn_AnonymousCustomer)
		Me.tabAtencion.Controls.Add(Me.btn_clean)
		Me.tabAtencion.Location = New System.Drawing.Point(4, 35)
		Me.tabAtencion.Name = "tabAtencion"
		Me.tabAtencion.Padding = New System.Windows.Forms.Padding(3)
		Me.tabAtencion.Size = New System.Drawing.Size(912, 474)
		Me.tabAtencion.TabIndex = 0
		Me.tabAtencion.Text = "Atención"
		Me.tabAtencion.UseVisualStyleBackColor = True
		'
		'lbl_loading
		'
		Me.lbl_loading.AutoSize = True
		Me.lbl_loading.Font = New System.Drawing.Font("Roboto", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_loading.ForeColor = System.Drawing.SystemColors.ControlDark
		Me.lbl_loading.Location = New System.Drawing.Point(340, 89)
		Me.lbl_loading.Name = "lbl_loading"
		Me.lbl_loading.Size = New System.Drawing.Size(113, 17)
		Me.lbl_loading.TabIndex = 27
		Me.lbl_loading.Text = "Consultando DNI..."
		Me.lbl_loading.Visible = False
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
						Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.txtsearch)
		Me.Panel1.Location = New System.Drawing.Point(338, 36)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Padding = New System.Windows.Forms.Padding(8, 12, 8, 12)
		Me.Panel1.Size = New System.Drawing.Size(550, 50)
		Me.Panel1.TabIndex = 26
		'
		'txtsearch
		'
		Me.txtsearch.BackColor = System.Drawing.Color.White
		Me.txtsearch.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtsearch.Dock = System.Windows.Forms.DockStyle.Fill
		Me.txtsearch.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtsearch.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.txtsearch.Location = New System.Drawing.Point(8, 12)
		Me.txtsearch.Margin = New System.Windows.Forms.Padding(24, 3, 3, 3)
		Me.txtsearch.MaxLength = 8
		Me.txtsearch.Name = "txtsearch"
		Me.txtsearch.ReadOnly = True
		Me.txtsearch.Size = New System.Drawing.Size(532, 26)
		Me.txtsearch.TabIndex = 8
		'
		'flowPanelButtons
		'
		Me.flowPanelButtons.Anchor = System.Windows.Forms.AnchorStyles.Left
		Me.flowPanelButtons.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
		Me.flowPanelButtons.Controls.Add(Me.btn_num07)
		Me.flowPanelButtons.Controls.Add(Me.btn_num08)
		Me.flowPanelButtons.Controls.Add(Me.btn_num09)
		Me.flowPanelButtons.Controls.Add(Me.btn_num04)
		Me.flowPanelButtons.Controls.Add(Me.btn_num05)
		Me.flowPanelButtons.Controls.Add(Me.btn_num06)
		Me.flowPanelButtons.Controls.Add(Me.btn_num01)
		Me.flowPanelButtons.Controls.Add(Me.btn_num02)
		Me.flowPanelButtons.Controls.Add(Me.btn_num03)
		Me.flowPanelButtons.Controls.Add(Me.btn_num00)
		Me.flowPanelButtons.Controls.Add(Me.btn_delete)
		Me.flowPanelButtons.Controls.Add(Me.btn_done)
		Me.flowPanelButtons.Location = New System.Drawing.Point(23, 14)
		Me.flowPanelButtons.Name = "flowPanelButtons"
		Me.flowPanelButtons.Padding = New System.Windows.Forms.Padding(12)
		Me.flowPanelButtons.Size = New System.Drawing.Size(285, 358)
		Me.flowPanelButtons.TabIndex = 25
		'
		'btn_num07
		'
		Me.btn_num07.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num07.FlatAppearance.BorderSize = 0
		Me.btn_num07.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num07.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num07.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num07.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num07.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num07.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num07.Location = New System.Drawing.Point(15, 15)
		Me.btn_num07.Name = "btn_num07"
		Me.btn_num07.Size = New System.Drawing.Size(80, 78)
		Me.btn_num07.TabIndex = 27
		Me.btn_num07.Tag = "7"
		Me.btn_num07.Text = "7"
		Me.btn_num07.UseVisualStyleBackColor = False
		'
		'btn_num08
		'
		Me.btn_num08.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num08.FlatAppearance.BorderSize = 0
		Me.btn_num08.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num08.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num08.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num08.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num08.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num08.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num08.Location = New System.Drawing.Point(101, 15)
		Me.btn_num08.Name = "btn_num08"
		Me.btn_num08.Size = New System.Drawing.Size(80, 78)
		Me.btn_num08.TabIndex = 28
		Me.btn_num08.Tag = "8"
		Me.btn_num08.Text = "8"
		Me.btn_num08.UseVisualStyleBackColor = False
		'
		'btn_num09
		'
		Me.btn_num09.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num09.FlatAppearance.BorderSize = 0
		Me.btn_num09.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num09.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num09.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num09.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num09.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num09.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num09.Location = New System.Drawing.Point(187, 15)
		Me.btn_num09.Name = "btn_num09"
		Me.btn_num09.Size = New System.Drawing.Size(80, 78)
		Me.btn_num09.TabIndex = 29
		Me.btn_num09.Tag = "9"
		Me.btn_num09.Text = "9"
		Me.btn_num09.UseVisualStyleBackColor = False
		'
		'btn_num04
		'
		Me.btn_num04.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num04.FlatAppearance.BorderSize = 0
		Me.btn_num04.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num04.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num04.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num04.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num04.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num04.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num04.Location = New System.Drawing.Point(15, 99)
		Me.btn_num04.Name = "btn_num04"
		Me.btn_num04.Size = New System.Drawing.Size(80, 78)
		Me.btn_num04.TabIndex = 30
		Me.btn_num04.Tag = "4"
		Me.btn_num04.Text = "4"
		Me.btn_num04.UseVisualStyleBackColor = False
		'
		'btn_num05
		'
		Me.btn_num05.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num05.FlatAppearance.BorderSize = 0
		Me.btn_num05.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num05.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num05.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num05.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num05.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num05.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num05.Location = New System.Drawing.Point(101, 99)
		Me.btn_num05.Name = "btn_num05"
		Me.btn_num05.Size = New System.Drawing.Size(80, 78)
		Me.btn_num05.TabIndex = 31
		Me.btn_num05.Tag = "5"
		Me.btn_num05.Text = "5"
		Me.btn_num05.UseVisualStyleBackColor = False
		'
		'btn_num06
		'
		Me.btn_num06.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num06.FlatAppearance.BorderSize = 0
		Me.btn_num06.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num06.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num06.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num06.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num06.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num06.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num06.Location = New System.Drawing.Point(187, 99)
		Me.btn_num06.Name = "btn_num06"
		Me.btn_num06.Size = New System.Drawing.Size(80, 78)
		Me.btn_num06.TabIndex = 32
		Me.btn_num06.Tag = "6"
		Me.btn_num06.Text = "6"
		Me.btn_num06.UseVisualStyleBackColor = False
		'
		'btn_num01
		'
		Me.btn_num01.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num01.FlatAppearance.BorderSize = 0
		Me.btn_num01.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num01.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num01.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num01.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num01.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num01.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num01.Location = New System.Drawing.Point(15, 183)
		Me.btn_num01.Name = "btn_num01"
		Me.btn_num01.Size = New System.Drawing.Size(80, 78)
		Me.btn_num01.TabIndex = 33
		Me.btn_num01.Tag = "1"
		Me.btn_num01.Text = "1"
		Me.btn_num01.UseVisualStyleBackColor = False
		'
		'btn_num02
		'
		Me.btn_num02.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num02.FlatAppearance.BorderSize = 0
		Me.btn_num02.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num02.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num02.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num02.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num02.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num02.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num02.Location = New System.Drawing.Point(101, 183)
		Me.btn_num02.Name = "btn_num02"
		Me.btn_num02.Size = New System.Drawing.Size(80, 78)
		Me.btn_num02.TabIndex = 34
		Me.btn_num02.Tag = "2"
		Me.btn_num02.Text = "2"
		Me.btn_num02.UseVisualStyleBackColor = False
		'
		'btn_num03
		'
		Me.btn_num03.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num03.FlatAppearance.BorderSize = 0
		Me.btn_num03.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num03.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num03.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num03.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num03.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num03.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num03.Location = New System.Drawing.Point(187, 183)
		Me.btn_num03.Name = "btn_num03"
		Me.btn_num03.Size = New System.Drawing.Size(80, 78)
		Me.btn_num03.TabIndex = 35
		Me.btn_num03.Tag = "3"
		Me.btn_num03.Text = "3"
		Me.btn_num03.UseVisualStyleBackColor = False
		'
		'btn_num00
		'
		Me.btn_num00.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(7, Byte), Integer))
		Me.btn_num00.FlatAppearance.BorderSize = 0
		Me.btn_num00.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num00.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num00.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.btn_num00.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.btn_num00.Font = New System.Drawing.Font("Roboto Medium", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_num00.ForeColor = System.Drawing.Color.FromArgb(CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
		Me.btn_num00.Location = New System.Drawing.Point(15, 267)
		Me.btn_num00.Name = "btn_num00"
		Me.btn_num00.Size = New System.Drawing.Size(80, 78)
		Me.btn_num00.TabIndex = 36
		Me.btn_num00.Tag = "0"
		Me.btn_num00.Text = "0"
		Me.btn_num00.UseVisualStyleBackColor = False
		'
		'btn_delete
		'
		Me.btn_delete.Depth = 0
		Me.btn_delete.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_delete.Location = New System.Drawing.Point(101, 267)
		Me.btn_delete.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_delete.Name = "btn_delete"
		Me.btn_delete.Primary = True
		Me.btn_delete.Size = New System.Drawing.Size(80, 80)
		Me.btn_delete.TabIndex = 15
		Me.btn_delete.Text = "Borrar"
		Me.btn_delete.UseVisualStyleBackColor = True
		'
		'btn_done
		'
		Me.btn_done.Depth = 0
		Me.btn_done.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btn_done.Location = New System.Drawing.Point(187, 267)
		Me.btn_done.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_done.Name = "btn_done"
		Me.btn_done.Primary = True
		Me.btn_done.Size = New System.Drawing.Size(80, 80)
		Me.btn_done.TabIndex = 15
		Me.btn_done.Text = "ACEPTAR"
		Me.btn_done.UseVisualStyleBackColor = True
		'
		'lbl_clientLastName
		'
		Me.lbl_clientLastName.AutoSize = True
		Me.lbl_clientLastName.Depth = 0
		Me.lbl_clientLastName.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_clientLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lbl_clientLastName.Location = New System.Drawing.Point(428, 185)
		Me.lbl_clientLastName.MouseState = MaterialSkin.MouseState.HOVER
		Me.lbl_clientLastName.Name = "lbl_clientLastName"
		Me.lbl_clientLastName.Size = New System.Drawing.Size(30, 19)
		Me.lbl_clientLastName.TabIndex = 24
		Me.lbl_clientLastName.Text = "___"
		'
		'lbl_clientName
		'
		Me.lbl_clientName.AutoSize = True
		Me.lbl_clientName.Depth = 0
		Me.lbl_clientName.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_clientName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lbl_clientName.Location = New System.Drawing.Point(428, 148)
		Me.lbl_clientName.MouseState = MaterialSkin.MouseState.HOVER
		Me.lbl_clientName.Name = "lbl_clientName"
		Me.lbl_clientName.Size = New System.Drawing.Size(30, 19)
		Me.lbl_clientName.TabIndex = 23
		Me.lbl_clientName.Text = "___"
		'
		'MaterialLabel8
		'
		Me.MaterialLabel8.AutoSize = True
		Me.MaterialLabel8.Depth = 0
		Me.MaterialLabel8.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel8.Location = New System.Drawing.Point(342, 185)
		Me.MaterialLabel8.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel8.Name = "MaterialLabel8"
		Me.MaterialLabel8.Size = New System.Drawing.Size(80, 19)
		Me.MaterialLabel8.TabIndex = 22
		Me.MaterialLabel8.Text = "Apellidos: "
		'
		'MaterialLabel9
		'
		Me.MaterialLabel9.AutoSize = True
		Me.MaterialLabel9.Depth = 0
		Me.MaterialLabel9.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel9.Location = New System.Drawing.Point(343, 148)
		Me.MaterialLabel9.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel9.Name = "MaterialLabel9"
		Me.MaterialLabel9.Size = New System.Drawing.Size(79, 19)
		Me.MaterialLabel9.TabIndex = 21
		Me.MaterialLabel9.Text = "Nombres: "
		'
		'MaterialLabel10
		'
		Me.MaterialLabel10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
						Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.MaterialLabel10.AutoSize = True
		Me.MaterialLabel10.Depth = 0
		Me.MaterialLabel10.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.MaterialLabel10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.MaterialLabel10.Location = New System.Drawing.Point(334, 14)
		Me.MaterialLabel10.MouseState = MaterialSkin.MouseState.HOVER
		Me.MaterialLabel10.Name = "MaterialLabel10"
		Me.MaterialLabel10.Size = New System.Drawing.Size(34, 19)
		Me.MaterialLabel10.TabIndex = 20
		Me.MaterialLabel10.Text = "DNI"
		'
		'btn_AnonymousCustomer
		'
		Me.btn_AnonymousCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btn_AnonymousCustomer.Depth = 0
		Me.btn_AnonymousCustomer.Location = New System.Drawing.Point(706, 411)
		Me.btn_AnonymousCustomer.Margin = New System.Windows.Forms.Padding(8, 3, 24, 12)
		Me.btn_AnonymousCustomer.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_AnonymousCustomer.Name = "btn_AnonymousCustomer"
		Me.btn_AnonymousCustomer.Primary = True
		Me.btn_AnonymousCustomer.Size = New System.Drawing.Size(182, 48)
		Me.btn_AnonymousCustomer.TabIndex = 19
		Me.btn_AnonymousCustomer.Text = "Cliente Anónimo"
		Me.btn_AnonymousCustomer.UseVisualStyleBackColor = True
		'
		'btn_clean
		'
		Me.btn_clean.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btn_clean.AutoSize = True
		Me.btn_clean.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.btn_clean.Depth = 0
		Me.btn_clean.Location = New System.Drawing.Point(23, 417)
		Me.btn_clean.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
		Me.btn_clean.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_clean.Name = "btn_clean"
		Me.btn_clean.Primary = False
		Me.btn_clean.Size = New System.Drawing.Size(76, 36)
		Me.btn_clean.TabIndex = 18
		Me.btn_clean.Text = "Reiniciar"
		Me.btn_clean.UseVisualStyleBackColor = True
		'
		'TabRequisitos
		'
		Me.TabRequisitos.Controls.Add(Me.TableLayoutPanel1)
		Me.TabRequisitos.Location = New System.Drawing.Point(4, 35)
		Me.TabRequisitos.Name = "TabRequisitos"
		Me.TabRequisitos.Padding = New System.Windows.Forms.Padding(3)
		Me.TabRequisitos.Size = New System.Drawing.Size(912, 474)
		Me.TabRequisitos.TabIndex = 1
		Me.TabRequisitos.Text = "Requisitos"
		Me.TabRequisitos.UseVisualStyleBackColor = True
		'
		'TableLayoutPanel1
		'
		Me.TableLayoutPanel1.ColumnCount = 2
		Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.61187!))
		Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.38813!))
		Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
		Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 1, 0)
		Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 6)
		Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
		Me.TableLayoutPanel1.RowCount = 1
		Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
		Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
		Me.TableLayoutPanel1.Size = New System.Drawing.Size(876, 460)
		Me.TableLayoutPanel1.TabIndex = 0
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.flowPanelRequirements)
		Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel2.Location = New System.Drawing.Point(3, 3)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(340, 454)
		Me.Panel2.TabIndex = 0
		'
		'flowPanelRequirements
		'
		Me.flowPanelRequirements.AutoScroll = True
		Me.flowPanelRequirements.Dock = System.Windows.Forms.DockStyle.Left
		Me.flowPanelRequirements.Location = New System.Drawing.Point(0, 0)
		Me.flowPanelRequirements.Margin = New System.Windows.Forms.Padding(0, 0, 48, 0)
		Me.flowPanelRequirements.Name = "flowPanelRequirements"
		Me.flowPanelRequirements.Padding = New System.Windows.Forms.Padding(4)
		Me.flowPanelRequirements.Size = New System.Drawing.Size(340, 454)
		Me.flowPanelRequirements.TabIndex = 13
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.btn_printer)
		Me.Panel3.Controls.Add(Me.RichTextBox1)
		Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel3.Location = New System.Drawing.Point(349, 3)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(524, 454)
		Me.Panel3.TabIndex = 1
		'
		'btn_printer
		'
		Me.btn_printer.Depth = 2
		Me.btn_printer.Location = New System.Drawing.Point(374, 403)
		Me.btn_printer.MouseState = MaterialSkin.MouseState.HOVER
		Me.btn_printer.Name = "btn_printer"
		Me.btn_printer.Primary = True
		Me.btn_printer.Size = New System.Drawing.Size(146, 48)
		Me.btn_printer.TabIndex = 1
		Me.btn_printer.Text = "IMPRIMIR"
		Me.btn_printer.UseVisualStyleBackColor = True
		'
		'RichTextBox1
		'
		Me.RichTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(249, Byte), Integer), CType(CType(249, Byte), Integer))
		Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.RichTextBox1.DetectUrls = False
		Me.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Top
		Me.RichTextBox1.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.RichTextBox1.Location = New System.Drawing.Point(0, 0)
		Me.RichTextBox1.Name = "RichTextBox1"
		Me.RichTextBox1.ReadOnly = True
		Me.RichTextBox1.Size = New System.Drawing.Size(524, 397)
		Me.RichTextBox1.TabIndex = 0
		Me.RichTextBox1.Text = ""
		'
		'tablist
		'
		Me.tablist.Controls.Add(Me.dgv_tickets)
		Me.tablist.Location = New System.Drawing.Point(4, 35)
		Me.tablist.Name = "tablist"
		Me.tablist.Padding = New System.Windows.Forms.Padding(3)
		Me.tablist.Size = New System.Drawing.Size(912, 474)
		Me.tablist.TabIndex = 2
		Me.tablist.Text = "Tickets Emitidos"
		Me.tablist.UseVisualStyleBackColor = True
		'
		'dgv_tickets
		'
		Me.dgv_tickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.dgv_tickets.Dock = System.Windows.Forms.DockStyle.Fill
		Me.dgv_tickets.Location = New System.Drawing.Point(3, 3)
		Me.dgv_tickets.MultiSelect = False
		Me.dgv_tickets.Name = "dgv_tickets"
		Me.dgv_tickets.ReadOnly = True
		Me.dgv_tickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
		Me.dgv_tickets.Size = New System.Drawing.Size(906, 468)
		Me.dgv_tickets.TabIndex = 0
		'
		'Panel4
		'
		Me.Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel4.BackColor = System.Drawing.Color.Transparent
		Me.Panel4.Controls.Add(Me.lbl_user)
		Me.Panel4.Controls.Add(Me.lbl_role)
		Me.Panel4.Controls.Add(Me.PictureBox1)
		Me.Panel4.Location = New System.Drawing.Point(579, 24)
		Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Padding = New System.Windows.Forms.Padding(2)
		Me.Panel4.Size = New System.Drawing.Size(337, 40)
		Me.Panel4.TabIndex = 2
		'
		'lbl_user
		'
		Me.lbl_user.Dock = System.Windows.Forms.DockStyle.Top
		Me.lbl_user.Font = New System.Drawing.Font("Roboto", 11.0!)
		Me.lbl_user.ForeColor = System.Drawing.Color.White
		Me.lbl_user.Location = New System.Drawing.Point(2, 2)
		Me.lbl_user.Margin = New System.Windows.Forms.Padding(0)
		Me.lbl_user.Name = "lbl_user"
		Me.lbl_user.Size = New System.Drawing.Size(298, 20)
		Me.lbl_user.TabIndex = 1
		Me.lbl_user.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'lbl_role
		'
		Me.lbl_role.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.lbl_role.Font = New System.Drawing.Font("Roboto", 9.0!)
		Me.lbl_role.ForeColor = System.Drawing.Color.White
		Me.lbl_role.Location = New System.Drawing.Point(2, 20)
		Me.lbl_role.Margin = New System.Windows.Forms.Padding(0)
		Me.lbl_role.Name = "lbl_role"
		Me.lbl_role.Size = New System.Drawing.Size(298, 18)
		Me.lbl_role.TabIndex = 2
		Me.lbl_role.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'PictureBox1
		'
		Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Right
		Me.PictureBox1.Image = Global.COrientador.My.Resources.Resources.icon_material_action_perm_identity
		Me.PictureBox1.Location = New System.Drawing.Point(300, 2)
		Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Size = New System.Drawing.Size(35, 36)
		Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
		Me.PictureBox1.TabIndex = 0
		Me.PictureBox1.TabStop = False
		'
		'lbl_local
		'
		Me.lbl_local.BackColor = System.Drawing.Color.Transparent
		Me.lbl_local.Font = New System.Drawing.Font("Roboto", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbl_local.ForeColor = System.Drawing.SystemColors.ButtonHighlight
		Me.lbl_local.Location = New System.Drawing.Point(14, 5)
		Me.lbl_local.Name = "lbl_local"
		Me.lbl_local.Size = New System.Drawing.Size(289, 15)
		Me.lbl_local.TabIndex = 3
		'
		'frm_inicio
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 26.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(920, 640)
		Me.Controls.Add(Me.lbl_local)
		Me.Controls.Add(Me.Panel4)
		Me.Controls.Add(Me.tabControl)
		Me.Controls.Add(Me.ms_selector)
		Me.Font = New System.Drawing.Font("Roboto", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margin = New System.Windows.Forms.Padding(6)
		Me.MinimumSize = New System.Drawing.Size(920, 640)
		Me.Name = "frm_inicio"
		Me.Padding = New System.Windows.Forms.Padding(0, 64, 0, 0)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "INICIO"
		Me.tabControl.ResumeLayout(False)
		Me.tabAtencion.ResumeLayout(False)
		Me.tabAtencion.PerformLayout()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.flowPanelButtons.ResumeLayout(False)
		Me.TabRequisitos.ResumeLayout(False)
		Me.TableLayoutPanel1.ResumeLayout(False)
		Me.Panel2.ResumeLayout(False)
		Me.Panel3.ResumeLayout(False)
		Me.tablist.ResumeLayout(False)
		CType(Me.dgv_tickets, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel4.ResumeLayout(False)
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents ms_selector As MaterialSkin.Controls.MaterialTabSelector
	Friend WithEvents tabControl As MaterialSkin.Controls.MaterialTabControl
	Friend WithEvents tabAtencion As TabPage
	Friend WithEvents Panel1 As Panel
	Friend WithEvents txtsearch As TextBox
	Friend WithEvents flowPanelButtons As FlowLayoutPanel
	Friend WithEvents lbl_clientLastName As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents lbl_clientName As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel8 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel9 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents MaterialLabel10 As MaterialSkin.Controls.MaterialLabel
	Friend WithEvents btn_AnonymousCustomer As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents btn_clean As MaterialSkin.Controls.MaterialFlatButton
	Friend WithEvents TabRequisitos As TabPage
	Friend WithEvents btn_num07 As Button
	Friend WithEvents btn_num08 As Button
	Friend WithEvents btn_num09 As Button
	Friend WithEvents btn_num04 As Button
	Friend WithEvents btn_num05 As Button
	Friend WithEvents btn_num06 As Button
	Friend WithEvents btn_num01 As Button
	Friend WithEvents btn_num02 As Button
	Friend WithEvents btn_num03 As Button
	Friend WithEvents btn_num00 As Button
	Friend WithEvents btn_delete As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents btn_done As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
	Friend WithEvents Panel2 As Panel
	Friend WithEvents Panel3 As Panel
	Friend WithEvents btn_printer As MaterialSkin.Controls.MaterialRaisedButton
	Friend WithEvents RichTextBox1 As RichTextBox
	Friend WithEvents Panel4 As Panel
	Friend WithEvents lbl_user As Label
	Friend WithEvents lbl_role As Label
	Friend WithEvents PictureBox1 As PictureBox
	Friend WithEvents lbl_local As Label
	Friend WithEvents lbl_loading As Label
	Friend WithEvents flowPanelRequirements As FlowLayoutPanel
	Friend WithEvents tablist As TabPage
	Friend WithEvents dgv_tickets As DataGridView
End Class
