﻿Imports System.Collections.Specialized
Imports COrientador
Imports MaterialSkin.Controls
Imports RestSharp

Public Class frm_preferencias
	Private Sub frm_preferencias_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Config.FormSkin(Me)

		Me.lbl_local.Text = Config.GetSetting(Variables.LocalKeyConfig) + " - " + Config.GetSetting(Variables.LocalStationKeyConfig)
		Me.lbl_user.Text = Config.GetSetting(Variables.UserNameKeyConfig) + " " + Config.GetSetting(Variables.UserLastNameKeyConfig)
		Me.lbl_role.Text = Config.GetSetting(Variables.UserRoleKeyConfig)

		Me.lbl_servicio.Text = Config.GetSetting(Variables.SelectedServiceKeyConfig)
		Me.lbl_cliente.Text = Config.GetSetting(Variables.SelectedClientKeyConfig) + " | DNI: " + Config.GetSetting(Variables.SelectedDNIKeyConfig)


		Me.flowPanelPreferences.Controls.Add(Element.LabelAction("Cargando Opciones...", "lbl_loading_01"))

		Dim QSync As New Sync(Me)

		QSync.Init(New oParamsSync With {
				.type = Method.GET,
				.resource = "options/preferences",
				.format = DataFormat.Json,
				.data = Nothing,
				.DataDeserialize = New List(Of oPreferences),
				.Success = AddressOf loadPreferences
			})

	End Sub

	Private Sub loadPreferences(obj As oResponse)

		Dim data As List(Of oPreferences) = obj.data
		Dim count As Integer = 1

		Me.flowPanelPreferences.Controls.Clear()

		If obj.data Is Nothing Then
			Me.flowPanelPreferences.Controls.Add(Element.LabelAction("No existen preferencias asignadas", "lbl_loading_02"))
			Return
		End If

		If data.Count = 0 Then
			Me.flowPanelPreferences.Controls.Add(Element.LabelAction("No existen preferencias asignadas", "lbl_loading_02"))
			Return
		End If

		For Each row As oPreferences In data

			Dim text As String = row.preferencia

			Dim name As String = "btn_preference_" + count.ToString()

			Dim btn As MaterialRaisedButton = Element.ButtonOption(text, name)

			AddHandler btn.Click, AddressOf btn_group_Click

			btn.Tag = row
			btn.MaximumSize = New System.Drawing.Size(240, 70)
			btn.MinimumSize = New System.Drawing.Size(240, 70)
			btn.Padding = New System.Windows.Forms.Padding(12)

			Element.setToolTip(btn, row.observacion, "Más Información", ToolTipIcon.Info)

			Me.flowPanelPreferences.Controls.Add(btn)

			count += 1

		Next

	End Sub

	Private Sub btn_group_Click(sender As Object, e As EventArgs)

		Dim btn As MaterialRaisedButton = sender
		Dim row As oPreferences = btn.Tag

		If Config.GetSetting(Variables.AskBeforePrinting) = 1 Then

			Dim result = MessageBox.Show(Me, "¿DESEA IMPRIMIR EL TICKET?", "CONFIRMAR IMPRESIÓN", MsgBoxStyle.OkCancel, MessageBoxIcon.Question)

			If result = MsgBoxResult.Cancel Then
				Return
			End If

		End If

		Dim QSync As New Sync(Me)

		Dim data As New StringDictionary

		'servicio
		data.Add("sr", Config.GetSetting(Variables.SelectedServiceIdKeyConfig))
		'preferencia
		data.Add("pf", row.rowid)
		'cliente dni
		data.Add("cd", Config.GetSetting(Variables.SelectedDNIKeyConfig))
		'cliente
		data.Add("c", Config.GetSetting(Variables.SelectedClientKeyConfig))

		QSync.Init(New oParamsSync With {
			.type = Method.POST,
			.resource = "ticket/generate",
			.format = DataFormat.Json,
			.data = data,
			.DataDeserialize = New oTicket,
			.Success = AddressOf sendTicket
		})

	End Sub

	Private Sub sendTicket(obj As oResponse)

		If obj.data Is Nothing Then
			MessageBox.Show("EL TICKET NO HA PODIDO SER GENERADO VUELVA A INTENTARLO")
			Return
		End If

		Dim ticket As oTicket = obj.data
		Dim cTicket As New Ticket

		cTicket.corre = ticket.corr
		cTicket.abrev = ticket.abrev
		cTicket.date = ticket.date
		cTicket.time = ticket.time
		cTicket.servicio = ticket.serv

		cTicket.init(Me)

		Dim frm As New frm_inicio

		Config.UpdateSettings(Variables.SelectedClientKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedDNIKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedServiceIdKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedServiceKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedPreferenceKeyConfig, String.Empty)

		frm.WindowState = Me.WindowState
		frm.Show()

		Me.Close()

	End Sub

	Private Sub btn_back_Click(sender As Object, e As EventArgs) Handles btn_back.Click

		Dim frm As New frm_servicios

		frm.WindowState = Me.WindowState
		frm.Show()

		Me.Close()

	End Sub

	Private Sub btn_clean_Click(sender As Object, e As EventArgs) Handles btn_clean.Click

		Me.Enabled = False

		Config.UpdateSettings(Variables.SelectedClientKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedDNIKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedServiceIdKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedServiceKeyConfig, String.Empty)
		Config.UpdateSettings(Variables.SelectedPreferenceKeyConfig, String.Empty)

		Dim frm As New frm_inicio

		frm.WindowState = Me.WindowState

		frm.Show()

		Me.Close()

	End Sub

End Class