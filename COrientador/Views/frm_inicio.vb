﻿Imports System.Collections.Specialized
Imports System.Drawing.Printing
Imports System.Windows.Forms
Imports COrientador
Imports MaterialSkin
Imports MaterialSkin.Controls
Imports RestSharp

Public Class frm_inicio
	Private Sub frm_inicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Config.FormSkin(Me)

		Me.lbl_local.Text = Config.GetSetting(Variables.LocalKeyConfig)
		Me.lbl_user.Text = Config.GetSetting(Variables.UserNameKeyConfig) + " " + Config.GetSetting(Variables.UserLastNameKeyConfig)
		Me.lbl_role.Text = Config.GetSetting(Variables.UserRoleKeyConfig)

		Me.txtsearch.Focus()

	End Sub

	Private Sub btn_nums_Click(sender As Object, e As EventArgs) Handles btn_num00.Click, btn_num01.Click, btn_num02.Click, btn_num03.Click, btn_num04.Click, btn_num05.Click, btn_num06.Click, btn_num07.Click, btn_num08.Click, btn_num09.Click

		If Me.txtsearch.Text.Length < 8 Then

			Dim meBtn As Button = sender
			Me.txtsearch.Text += meBtn.Tag.ToString()

			If Me.txtsearch.Text = "99999999" Then

				Dim result As MsgBoxResult = MsgBox("Cliente: Anónimo." + vbCrLf + "DNI: 99999999", MsgBoxStyle.OkCancel, "Atención")

				If result = MsgBoxResult.Ok Then

					Config.UpdateSettings(Variables.SelectedClientKeyConfig, "Cliente Anónimo")
					Config.UpdateSettings(Variables.SelectedDNIKeyConfig, "99999999")

					Me.ContinueProccess()

				Else

					Me.txtsearch.Clear()

				End If

			End If

		End If

		If Me.txtsearch.Text.Length = 8 Then

			Me.flowPanelButtons.Enabled = False
			Me.btn_clean.Enabled = False
			Me.btn_AnonymousCustomer.Enabled = False
			Me.lbl_loading.Visible = True


			Dim QSync As New Sync(Me)

			Dim data As New StringDictionary

			data.Add("q", Me.txtsearch.Text)

			QSync.Init(New oParamsSync With {
				.type = Method.GET,
				.resource = "options/customers",
				.format = DataFormat.Json,
				.data = data,
				.DataDeserialize = New oCustomer,
				.Success = AddressOf ConsultCustomer
			})

		End If

	End Sub

	Private Sub ConsultCustomer(obj As oResponse)

		If (obj.data Is Nothing) Then
			Me.flowPanelButtons.Enabled = True
			Me.btn_clean.Enabled = True
			Me.btn_AnonymousCustomer.Enabled = True
			Me.lbl_loading.Visible = False
			Me.txtsearch.Clear()
			Return
		End If

		Dim data As oCustomer = obj.data

		Me.lbl_clientName.Text = data.name
		Me.lbl_clientLastName.Text = data.lastname

		Dim nombre As String = "Cliente: " + data.name + " " + data.lastname + vbCrLf

		nombre += "DNI: " + data.dni + vbCrLf + vbCrLf + "¿Los datos son correctos?"

		Dim result As MsgBoxResult = MsgBox(nombre, MsgBoxStyle.OkCancel, "Atención")

		If result = MsgBoxResult.Ok Then

			Config.UpdateSettings(Variables.SelectedClientKeyConfig, data.name + " " + data.lastname)
			Config.UpdateSettings(Variables.SelectedDNIKeyConfig, data.dni)

			Me.ContinueProccess()

		Else

			Me.txtsearch.Clear()
			Me.lbl_clientName.Text = String.Empty
			Me.lbl_clientLastName.Text = String.Empty

		End If

		Me.flowPanelButtons.Enabled = True
		Me.btn_clean.Enabled = True
		Me.btn_AnonymousCustomer.Enabled = True
		Me.lbl_loading.Visible = False

	End Sub

	Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click

		If Me.txtsearch.Text.Length > 0 Then
			Me.txtsearch.Text = Me.txtsearch.Text.Substring(0, Me.txtsearch.Text.Length - 1)
		End If

	End Sub

	Private Sub btn_next_Click(sender As Object, e As EventArgs) Handles btn_AnonymousCustomer.Click

		Config.UpdateSettings(Variables.SelectedClientKeyConfig, "Cliente Anónimo")
		Config.UpdateSettings(Variables.SelectedDNIKeyConfig, "99999999")

		Me.ContinueProccess()

	End Sub

	Private Sub ContinueProccess()

		Dim frm As New frm_servicios

		frm.WindowState = Me.WindowState
		frm.Show()

		Me.Close()

	End Sub

	Private Sub tabControl_Selected(sender As Object, e As TabControlEventArgs) Handles tabControl.Selected
		Dim tabcontrol As MaterialSkin.Controls.MaterialTabControl = sender
		Dim page As TabPage = tabcontrol.SelectedTab

		If tabcontrol.SelectedTab.Name = Me.TabRequisitos.Name Then

			Dim QSync As New Sync(Me)

			Dim data As New StringDictionary

			data.Add("q", Me.txtsearch.Text)

			QSync.Init(New oParamsSync With {
				.type = Method.GET,
				.resource = "options/requirements",
				.format = DataFormat.Json,
				.data = data,
				.DataDeserialize = New List(Of oRequirements),
				.Success = AddressOf getRequirements
			})

		End If

	End Sub

	Private Sub getRequirements(obj As oResponse)

		Dim data As List(Of oRequirements) = obj.data
		Dim count As Integer = 1

		Me.flowPanelRequirements.Controls.Clear()

		If obj.data Is Nothing Then
			Me.flowPanelRequirements.Controls.Add(Element.LabelAction("No existen preferencias asignadas", "lbl_loading_02"))
			Return
		End If

		If data.Count = 0 Then
			Me.flowPanelRequirements.Controls.Add(Element.LabelAction("No existen preferencias asignadas", "lbl_loading_02"))
			Return
		End If

		For Each row As oRequirements In data

			Dim text As String = row.nombre

			Dim name As String = "btn_preference_" + count.ToString()

			Dim btn As MaterialRaisedButton = Element.ButtonOption(text, name)

			AddHandler btn.Click, AddressOf btn_group_Click

			btn.Tag = row
			btn.MaximumSize = New System.Drawing.Size(240, 70)
			btn.MinimumSize = New System.Drawing.Size(240, 70)
			btn.Padding = New System.Windows.Forms.Padding(12)

			Element.setToolTip(btn, row.descripcion, "Más Información", ToolTipIcon.Info)

			Me.flowPanelRequirements.Controls.Add(btn)

			count += 1

		Next

	End Sub

	Private Sub btn_group_Click(sender As Object, e As EventArgs)

		Dim btn As MaterialRaisedButton = sender
		Dim requeriment As oRequirements = btn.Tag
		Dim items As List(Of oRequirementsList) = requeriment.items
		Dim len As Integer

		Me.RichTextBox1.Clear()
		Me.RichTextBox1.Tag = requeriment

		Me.RichTextBox1.AppendText(requeriment.titulo)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(Environment.NewLine)

		len = Me.RichTextBox1.TextLength

		Me.RichTextBox1.AppendText("REQUISITOS")
		Me.RichTextBox1.SelectionStart = len
		Me.RichTextBox1.SelectionLength = "REQUISITOS".Length
		Me.RichTextBox1.SelectionFont = New Font("Arial", 12, FontStyle.Bold)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(requeriment.descripcion)
		Me.RichTextBox1.AppendText(Environment.NewLine)
		Me.RichTextBox1.AppendText(Environment.NewLine)

		Dim count As Integer = 1

		For Each item As oRequirementsList In items
			Me.RichTextBox1.AppendText(count.ToString + "). " + item.requisito)
			Me.RichTextBox1.AppendText(Environment.NewLine)
			count += 1
		Next

	End Sub

	Private Sub btn_printer_Click(sender As Object, e As EventArgs) Handles btn_printer.Click

		Dim cTicket As New Requirement

		cTicket.init(Me.RichTextBox1.Tag)

	End Sub
End Class