﻿Imports System.Collections.Specialized
Imports System.Drawing.Printing
Imports RestSharp
Imports MaterialSkin

Public Class frm_config

	Public meParen As frm_login

	Private Sub frm_config_Load(sender As Object, e As EventArgs) Handles MyBase.Load

		Config.FormSkin(Me)

		Me.loadPrinter(String.Empty)

		Me.txt_url.Text = Config.GetSetting(Variables.WebServiceKeyConfig)
		Me.txt_idapp.Text = Config.GetSetting(Variables.CodeKeyConfig)
		Me.cbo_protocols.SelectedItem = Config.GetSetting(Variables.ProtocolKeyConfig)
		Me.cbo_scanners.SelectedItem = Config.GetSetting(Variables.ScannerKeyConfig)
		Me.cbo_printers.SelectedItem = Config.GetSetting(Variables.PrinterKeyConfig)
		Me.lbl_local.Text = Config.GetSetting(Variables.LocalKeyConfig)
		Me.lbl_horario.Text = Config.GetSetting(Variables.LocalAddressKeyConfig)
		Me.chkb_confirmprinting.CheckState = Convert.ToInt32(Config.GetSetting(Variables.AskBeforePrinting))

		If Not String.IsNullOrEmpty(Config.GetSetting(Variables.IdKeyConfig)) Then
			Me.btn_registerApp.Enabled = False
		End If

	End Sub

	Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click

		If Not Me.validInputs() Then
			MessageBox.Show(Me, "Todos los elementos del grupo Sync son obligatorios", "Advertencia", MessageBoxIcon.Warning)
			Return
		End If

		Dim QSync As New Sync(Me)

		Dim data As New StringDictionary

		data.Add("scanner", Me.cbo_scanners.SelectedItem)
		data.Add("printer", Me.cbo_printers.SelectedItem)

		Me.btn_save.Enabled = False
		Me.UseWaitCursor = True

		QSync.Init(New oParamsSync With {
			.type = Method.POST,
			.resource = "config/components",
			.format = DataFormat.Json,
			.data = data,
			.DataDeserialize = New oInitApp(),
			.Success = AddressOf SaveConfiguration
		})

	End Sub


	Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click

		Me.Close()

	End Sub

	Private Sub btn_registerApp_Click(sender As Object, e As EventArgs) Handles btn_registerApp.Click

		If String.IsNullOrEmpty(Me.txt_idapp.Text.Trim()) Then
			MessageBox.Show(Me, "Debe agregar el código que el administrador le ha asignado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			Return
		End If

		If String.IsNullOrEmpty(txt_url.Text.Trim()) Then
			MessageBox.Show(Me, "Debe asignar el dominio que representa la web services de la organización", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			Return
		End If

		Dim QSync As New Sync(Me)

		Dim data As New StringDictionary

		data.Add("idapp", Me.txt_idapp.Text.Trim())
		data.Add("mac", Helpers.getMAC())
		data.Add("pcname", Helpers.getPcName())

		Me.UseWaitCursor = True
		Me.btn_registerApp.Enabled = False

		QSync.Init(New oParamsSync With {
			.base_url = (Me.cbo_protocols.SelectedItem + Me.txt_url.Text),
			.resource = "sync/register",
			.type = Method.POST,
			.format = DataFormat.Json,
			.data = data,
			.DataDeserialize = New oRegisterApp(),
			.Success = AddressOf VerificaZync
		})

	End Sub

	Private Sub txt_idapp_TextChanged(sender As Object, e As EventArgs) Handles txt_idapp.TextChanged

		If Not String.IsNullOrEmpty(Me.txt_idapp.Text.Trim()) Then

			If Not String.Equals(Me.txt_idapp.Text.Trim(), Config.GetSetting(Variables.CodeKeyConfig).Trim()) Then
				btn_registerApp.Enabled = True
			Else
				btn_registerApp.Enabled = False
			End If

		End If

	End Sub


	Private Sub chkb_confirmprinting_CheckedChanged(sender As Object, e As EventArgs) Handles chkb_confirmprinting.CheckedChanged

		Dim _this As MaterialSkin.Controls.MaterialCheckBox = sender

		Config.UpdateSettings(Variables.AskBeforePrinting, _this.CheckState)

	End Sub

#Region "FUNCIONES Y METODOS DEL FORMUARIO"

	Private Sub VerificaZync(response As oResponse)

		If Not response.error Then

			Dim data As oRegisterApp = response.data

			Config.baseUrl = (Me.cbo_protocols.SelectedItem + Me.txt_url.Text.Trim())

			'DATOS DE LA APLICACION REGISTRADA
			Config.UpdateSettings(Variables.IdKeyConfig, data.rowid)
			Config.UpdateSettings(Variables.TokenKeyConfig, data.token)
			Config.UpdateSettings(Variables.CodeKeyConfig, data.key)

			'DATOS DE CONEXION WEB SERVICE
			Config.UpdateSettings(Variables.ProtocolKeyConfig, Me.cbo_protocols.SelectedItem)
			Config.UpdateSettings(Variables.WebServiceKeyConfig, Me.txt_url.Text.Trim())

			'DATOS DEL LOCAL
			Config.UpdateSettings(Variables.LocalKeyConfig, data.local)
			Config.UpdateSettings(Variables.LocalIdKeyConfig, data.local_id)
			Config.UpdateSettings(Variables.LocalStationKeyConfig, data.puesto)
			Config.UpdateSettings(Variables.LocalAddressKeyConfig, data.direccion)

			Me.lbl_horario.Text = data.direccion
			Me.lbl_local.Text = data.local

			MessageBox.Show(Me, response.message, "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information)

		Else

			MessageBox.Show(response.message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

		End If

		Me.btn_registerApp.Enabled = True
		Me.UseWaitCursor = False

	End Sub

	Private Sub SaveConfiguration(response As oResponse)

		If (Not response.error) Then

			Config.UpdateSettings(Variables.ScannerKeyConfig, Me.cbo_scanners.SelectedItem)
			Config.UpdateSettings(Variables.PrinterKeyConfig, Me.cbo_printers.SelectedItem)

			meParen.conexionPermitida(True)

			Me.Close()

		Else

			MessageBox.Show(Me, response.message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

		End If

		Me.btn_save.Enabled = True
		Me.UseWaitCursor = False

	End Sub

	Private Function validInputs() As Boolean

		Dim result As Boolean = True

		If String.IsNullOrWhiteSpace(Me.txt_idapp.Text.Trim()) Then
			Me.txt_idapp.Focus()
			result = False
		End If

		If Me.cbo_protocols.SelectedIndex < 0 Then
			Me.cbo_protocols.Focus()
			result = False
		End If

		If String.IsNullOrWhiteSpace(Me.txt_url.Text.Trim()) Then
			Me.txt_url.Focus()
			result = False
		End If

		Return result

	End Function

	Private Sub loadPrinter(ByVal namePrinter As String)

		For Each pkInstalledPrinters In PrinterSettings.InstalledPrinters

			Me.cbo_printers.Items.Add(pkInstalledPrinters)

		Next

	End Sub

	Private Sub loadScanner(ByVal nameScanner As String)

		Me.cbo_scanners.Text = "Seleccione..."

	End Sub


#End Region


End Class